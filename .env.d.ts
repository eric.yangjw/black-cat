declare module '@env' {
  export const API_URL: 'http://localhost:3000';
  export const ENV: 'dev' | 'prod';
}
