import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
} from 'react-native';

const TestScreen = () => {
  // const backgroundImage = jest.mock('../assets/street-dance.jpg');

  return (
    <TouchableWithoutFeedback>
      <KeyboardAvoidingView behavior="padding">
        {/* <ImageBackground source={backgroundImage} style={styles.container}> */}
        <View style={styles.container}>
          <Text>This is a test</Text>
        </View>
        {/* </ImageBackground> */}
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default TestScreen;
