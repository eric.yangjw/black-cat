import React, {useRef, useState} from 'react';
import {
  FlatList,
  KeyboardAvoidingView,
  StyleSheet,
  View,
  Animated,
  Text,
} from 'react-native';

// Custom Components
import AppHeader from '../../components/app_header/AppHeader';
import EventBottomCommentBar from './components/EventBottomCommentBar';
import EventBottomActionBar from './components/EventBottomActionBar';
import EventDetailContent from './components/EventDetailContent';

// Model
import {AppEvent} from '../../model/appEvent';

export interface EventDetailProps {
  event: AppEvent;
}

const EventDetailScreen = () => {
  const [comment, setComment] = useState('');
  const [isCommenting, setIsCommenting] = useState(false);
  const contentRef = useRef<FlatList<any>>(null);
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const [toastMessage, setToastMsg] = useState('');

  const setUserCommenting = () => {
    contentRef.current?.scrollToEnd({animated: true});
    setIsCommenting(true);
  };

  const displayToast = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 2000,
      useNativeDriver: true,
    }).start(() =>
      setTimeout(
        () =>
          Animated.timing(fadeAnim, {
            toValue: 0,
            duration: 1000,
            useNativeDriver: true,
          }).start(),
        2000,
      ),
    );
  };

  const setToastMessage = (message: string) => setToastMsg(message);

  return (
    <KeyboardAvoidingView behavior="padding">
      <View style={styles.container}>
        <AppHeader />

        <EventDetailContent
          contentRef={contentRef}
          setComment={setComment}
          setIsCommenting={setIsCommenting}
        />
        <Animated.View style={[styles.toast, {opacity: fadeAnim}]}>
          <Text style={styles.toastText}>{toastMessage}</Text>
        </Animated.View>
        {isCommenting ? (
          <EventBottomCommentBar
            contentRef={contentRef}
            comment={comment}
            setComment={setComment}
            toggleCommenting={() => setIsCommenting(false)}
            displayToast={displayToast}
            setToastMessage={setToastMessage}
          />
        ) : (
          <EventBottomActionBar toggleCommenting={setUserCommenting} />
        )}
      </View>
    </KeyboardAvoidingView>
  );
};

export default EventDetailScreen;

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  toast: {
    height: 32,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E5F7A9DF',
    position: 'absolute',
    top: 130,
  },
  toastText: {
    color: '#8560A9',
    fontSize: 15,
    fontFamily: 'SourceSansPro_600SemiBold',
  },
  imageSection: {
    paddingVertical: 10,
    paddingHorizontal: 5,
    width: '100%',
  },
  image: {
    marginHorizontal: 10,
  },
});
