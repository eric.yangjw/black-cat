import React, {ReactNode, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Avatar} from 'react-native-elements/dist/avatar/Avatar';

// Custom Components
import {CheckLogo, CheckOutlineLogo} from '../../../../common/SVG';

// Redux
import {RootState} from '../../../../redux/store';
import {useSelector} from 'react-redux';
import {Icon} from 'react-native-elements';
import {TouchableOpacity} from 'react-native-gesture-handler';

const PARTICIPANT_WIDTH = 270;
const AVATAR_WIDTH = 35;
const PREVIEW_COUNT = Math.floor(PARTICIPANT_WIDTH / AVATAR_WIDTH) - 2;

const EventGoingSection = () => {
  const {event} = useSelector((state: RootState) => state.event);
  const authUser = useSelector((state: RootState) => state.auth.authUser);
  const [hasExpanded, setExpanded] = useState(false);

  const getGoingLogo = (): ReactNode =>
    event!.goings.filter(going => going.id === authUser?.id).length > 0 ? (
      <CheckLogo />
    ) : (
      <CheckOutlineLogo />
    );

  const toggleSectionHeight = () => setExpanded(!hasExpanded);

  return (
    <View style={[styles.container, event!.goings.length > 6 ? {} : {}]}>
      <View style={styles.status}>
        {getGoingLogo()}
        <Text>{`${event!.goings.length} going`}</Text>
      </View>

      <View
        style={[
          styles.participants,
          {flexWrap: hasExpanded ? 'wrap' : 'nowrap'},
        ]}>
        {event!.goings.map((going, idx) => {
          if (!hasExpanded && idx < PREVIEW_COUNT) {
            return (
              <View
                key={'hasnotexpandedgoing' + idx.toString()}
                style={styles.avatar}>
                <Avatar rounded size="small" source={{uri: going.avatar}} />
              </View>
            );
          } else if (hasExpanded) {
            return (
              <View
                key={'hasexpandedgoing' + idx.toString()}
                style={styles.avatar}>
                <Avatar rounded size="small" source={{uri: going.avatar}} />
              </View>
            );
          }

          return <View key={'empty:' + idx.toString()} />;
        })}
        {event!.goings.length > PREVIEW_COUNT + 1 ? (
          <TouchableOpacity style={styles.more} onPress={toggleSectionHeight}>
            <Icon
              name={hasExpanded ? 'upcircleo' : 'downcircleo'}
              type="antdesign"
              size={30}
              color="#AC8EC9"
            />
          </TouchableOpacity>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
};

export default EventGoingSection;

const styles = StyleSheet.create({
  container: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
    width: 100,
  },
  status: {
    flexDirection: 'row',
    width: 100,
  },
  participants: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: PARTICIPANT_WIDTH,
  },
  avatar: {
    marginVertical: 3,
    marginHorizontal: 3,
    width: AVATAR_WIDTH,
  },
  more: {
    width: AVATAR_WIDTH,
    height: AVATAR_WIDTH,
    justifyContent: 'center',
  },
});
