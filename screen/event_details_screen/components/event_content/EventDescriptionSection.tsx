import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';

// Redux
import {useSelector} from 'react-redux';
import {RootState} from '../../../../redux/store';

const EventDescriptionSection = () => {
  const {event} = useSelector((state: RootState) => state.event);
  const [hasExpanded, setHasExpanded] = useState(false);

  const getText = () => (hasExpanded ? 'VIEW LESS' : 'VIEW ALL');

  const toggleExpanded = () => setHasExpanded(!hasExpanded);

  return (
    <View style={[styles.container, {height: !hasExpanded ? 140 : undefined}]}>
      <View style={styles.overlay}>
        <View style={styles.contentContainer}>
          <Text
            numberOfLines={!hasExpanded ? 5 : undefined}
            style={styles.content}>
            {event!.description}
          </Text>
        </View>

        <LinearGradient
          colors={
            hasExpanded
              ? ['transparent', 'transparent']
              : ['#FFFFFFAF', '#FFFFFFFF']
          }
          locations={[0.3, 0.6]}
          style={[
            styles.viewMoreSection,
            {
              position: hasExpanded ? 'relative' : 'absolute',
              bottom: !hasExpanded ? -20 : undefined,
            },
          ]}>
          <TouchableOpacity
            style={styles.viewMoreButton}
            onPress={toggleExpanded}>
            <Text style={styles.viewMoreText}>{getText()}</Text>
          </TouchableOpacity>
        </LinearGradient>
      </View>
    </View>
  );
};

export default EventDescriptionSection;

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  overlay: {position: 'relative'},
  contentContainer: {},
  content: {
    color: '#67616D',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 14,
    flexWrap: 'wrap',
  },
  viewMoreSection: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    width: '100%',
    height: 40,
  },
  viewMoreButton: {
    backgroundColor: '#D5EF7F',
    width: 70,
    height: 25,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewMoreText: {
    fontFamily: 'SourceSansPro_600SemiBold',
    color: '#67616D',
    fontSize: 10,
  },
});
