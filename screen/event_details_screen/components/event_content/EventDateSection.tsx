import React, {ReactNode} from 'react';
import {StyleSheet, Text, View} from 'react-native';

// Custom Components
import {FromDateLogo, ToDateLogo} from '../../../../common/SVG';

// Redux
import {useSelector} from 'react-redux';
import {RootState} from '../../../../redux/store';

const EventDateSection = () => {
  const {event} = useSelector((state: RootState) => state.event);

  const moment = require('moment');
  const getDate = (time: number) =>
    moment(new Date(time)).format('DD MMMM YYYY');
  const getTime = (time: number) => moment(new Date(time)).format('hh:mm');
  const getSuffix = (time: number) => moment(new Date(time)).format('a');

  const DateBox = ({children, date}: {children: ReactNode; date: number}) => (
    <View style={styles.start}>
      <View style={styles.dateLinear}>
        {children}
        <Text>{getDate(date)}</Text>
      </View>
      <View style={styles.timeLinear}>
        <Text style={styles.time}>{getTime(date)}</Text>
        <Text style={styles.timeSuffix}>{getSuffix(date)}</Text>
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <DateBox date={event!.beginTime}>
        <FromDateLogo />
      </DateBox>
      <View style={styles.divider} />
      <DateBox date={event!.endTime}>
        <ToDateLogo />
      </DateBox>
    </View>
  );
};

export default EventDateSection;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  start: {
    height: 80,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  timeLinear: {
    width: 80,
    flexDirection: 'row',
    alignItems: 'baseline',
    justifyContent: 'space-between',
  },
  time: {
    color: '#AECB4F',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 32,
  },
  timeSuffix: {
    color: '#AECB4F',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 16,
  },
  dateLinear: {flexDirection: 'row'},
  end: {
    height: 80,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  divider: {height: '70%', width: 1, backgroundColor: '#E8E8E8'},
});
