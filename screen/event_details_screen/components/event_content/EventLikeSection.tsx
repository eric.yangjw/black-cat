import React, {ReactNode, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Avatar} from 'react-native-elements/dist/avatar/Avatar';

// Custom Components
import {LikeLogo, LikeOutlineLogo} from '../../../../common/SVG';

// Redux
import {RootState} from '../../../../redux/store';
import {useSelector} from 'react-redux';
import {Icon} from 'react-native-elements';

const PARTICIPANT_WIDTH = 270;
const AVATAR_WIDTH = 35;
const PREVIEW_COUNT = Math.floor(PARTICIPANT_WIDTH / AVATAR_WIDTH) - 2;

const LikeSection = () => {
  const {event} = useSelector((state: RootState) => state.event);
  const authUser = useSelector((state: RootState) => state.auth.authUser);
  const [hasExpanded, setExpanded] = useState(false);

  const getLikeLogo = (): ReactNode =>
    event!.likes.filter(like => like.id === authUser?.id).length > 0 ? (
      <LikeLogo />
    ) : (
      <LikeOutlineLogo />
    );

  const getLikeCount = (): string => {
    if (event!.likes.length <= 1) return `${event!.likes.length} like`;
    return `${event!.likes.length} likes`;
  };

  const toggleSectionHeight = () => setExpanded(!hasExpanded);

  return (
    <View style={styles.container}>
      <View style={styles.status}>
        {getLikeLogo()}
        <Text>{getLikeCount()} </Text>
      </View>

      <View
        style={[
          styles.participants,
          {flexWrap: hasExpanded ? 'wrap' : 'nowrap'},
        ]}>
        {event!.likes.map((like, idx) => {
          if (!hasExpanded && idx < PREVIEW_COUNT) {
            return (
              <View
                key={'hasnotexpanded-' + idx.toString()}
                style={styles.avatar}>
                <Avatar rounded size="small" source={{uri: like.avatar}} />
              </View>
            );
          } else if (hasExpanded) {
            return (
              <View key={'hasexpanded-' + idx.toString()} style={styles.avatar}>
                <Avatar rounded size="small" source={{uri: like.avatar}} />
              </View>
            );
          }
          return <View key={'empty:' + idx.toString()} />;
        })}
        {event!.likes.length > PREVIEW_COUNT + 1 ? (
          <TouchableOpacity style={styles.more} onPress={toggleSectionHeight}>
            <Icon
              name={hasExpanded ? 'upcircleo' : 'downcircleo'}
              type="antdesign"
              size={30}
              color="#AC8EC9"
            />
          </TouchableOpacity>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
};

export default LikeSection;

const styles = StyleSheet.create({
  container: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  status: {
    flexDirection: 'row',
    width: 100,
  },
  participants: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: PARTICIPANT_WIDTH,
  },
  avatar: {
    marginVertical: 3,
    marginHorizontal: 3,
    width: AVATAR_WIDTH,
  },
  more: {
    width: AVATAR_WIDTH,
    height: AVATAR_WIDTH,
    justifyContent: 'center',
  },
});
