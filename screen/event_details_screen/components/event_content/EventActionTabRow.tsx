import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  CommentsLogo,
  CommentsOutlineLogo,
  DetailsLogo,
  DetailsOutlineLogo,
  ParticipantsLogo,
  ParticipantsOutlineLogo,
} from '../../../../common/SVG';
import {EventContentType} from '../../../../common/enum/contentType';

interface EventActionTabProps {
  toggleTab: (tab: EventContentType) => void;
  currentContent: EventContentType;
}

const EventActionTabRow = ({
  toggleTab,
  currentContent,
}: EventActionTabProps) => {
  return (
    <View style={styles.container}>
      {/* ===== Details Tab ===== */}
      <TouchableOpacity
        style={styles.tab}
        onPress={() => toggleTab(EventContentType.Details)}>
        {currentContent === EventContentType.Details ? (
          <DetailsLogo />
        ) : (
          <DetailsOutlineLogo />
        )}
        <Text>Details</Text>
      </TouchableOpacity>
      <View style={styles.divider} />

      {/* ===== Participant Tab ===== */}
      <TouchableOpacity
        style={styles.tab}
        onPress={() => toggleTab(EventContentType.Participants)}>
        {currentContent === EventContentType.Participants ? (
          <ParticipantsLogo />
        ) : (
          <ParticipantsOutlineLogo />
        )}
        <Text>Participants</Text>
      </TouchableOpacity>
      <View style={styles.divider} />

      {/* ===== Comment Tab ===== */}
      <TouchableOpacity
        style={styles.tab}
        onPress={() => toggleTab(EventContentType.Comments)}>
        {currentContent === EventContentType.Comments ? (
          <CommentsLogo />
        ) : (
          <CommentsOutlineLogo />
        )}
        <Text>Comments</Text>
      </TouchableOpacity>
    </View>
  );
};

export default EventActionTabRow;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#E8E8E8',
    alignItems: 'center',
  },
  tab: {
    height: 45,
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  divider: {height: '70%', width: 1, backgroundColor: '#E8E8E8'},
});
