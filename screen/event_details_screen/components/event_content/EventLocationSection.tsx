import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

// Redux
import {useSelector} from 'react-redux';
import {RootState} from '../../../../redux/store';

const map = require('../../../../assets/gmap.png');

const EventLocationSection = () => {
  const {event} = useSelector((state: RootState) => state.event);
  const [building, address] = event!.location.split(/\n/);

  return (
    <View style={styles.container}>
      <View style={styles.address}>
        <Text style={styles.streetName}>{building}</Text>
        <Text style={styles.streetAddress}>{address}</Text>
      </View>
      <Image style={styles.map} source={map} />
    </View>
  );
};

export default EventLocationSection;

const styles = StyleSheet.create({
  container: {paddingHorizontal: 15},
  address: {},
  streetName: {
    color: '#67616D',
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 16,
  },
  streetAddress: {
    color: '#67616D',
    fontSize: 14,
    fontFamily: 'SourceSansPro_400Regular',
  },
  map: {
    marginVertical: 15,
    height: 100,
    width: '100%',
    borderRadius: 5,
  },
});
