import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Avatar} from 'react-native-elements/dist/avatar/Avatar';
import {TouchableOpacity} from 'react-native-gesture-handler';

// Custom Components
import {ReplyLogo} from '../../../../common/SVG';

// Model
import {Comment} from '../../../../model/comment';

interface CommentProps {
  comment: Comment;
  replyToUser: (username: string) => void;
}

const EventComment = ({comment, replyToUser}: CommentProps) => {
  const getCreateTime = (time: number) => {
    const moment = require('moment');
    return moment(new Date(time)).fromNow();
  };
  return (
    <View style={styles.container}>
      <View style={styles.avatar}>
        <Avatar rounded size="small" source={{uri: comment.author.avatar}} />
      </View>
      <View style={styles.content}>
        <View style={styles.contentHeader}>
          <View style={styles.commeterContainer}>
            <Text style={styles.commenter}>{comment.author.username}</Text>
          </View>
          <View>
            <Text style={styles.timestamp}>
              {getCreateTime(comment.createTime)}
            </Text>
          </View>
        </View>
        <View>
          <Text style={styles.comment}>{comment.comment}</Text>
        </View>
      </View>
      <TouchableOpacity
        style={styles.reply}
        onPress={() => replyToUser(comment.author.username)}>
        <ReplyLogo />
      </TouchableOpacity>
    </View>
  );
};

export default EventComment;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  avatar: {flex: 1},
  content: {flex: 6},
  contentHeader: {flexDirection: 'row', alignItems: 'baseline'},
  commeterContainer: {marginRight: 10},
  timestampContainer: {},
  commenter: {
    color: '#8560A9',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 12,
  },
  timestamp: {
    marginRight: 100,
    color: '#BABABA',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 10,
  },
  comment: {
    color: '#67616D',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 14,
  },
  reply: {
    flex: 1,
    paddingTop: 10,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
