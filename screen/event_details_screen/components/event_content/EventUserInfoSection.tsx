import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Avatar} from 'react-native-elements';

// Redux
import {useSelector} from 'react-redux';
import {RootState} from '../../../../redux/store';

const EventUserInfoSection = () => {
  const {event} = useSelector((state: RootState) => state.event);

  const getPublishedDate = () => {
    const moment = require('moment');
    const time = moment(new Date(event!.createTime), 'YYYYMMDD').fromNow();
    return `Published ${time}`;
  };

  return (
    <View style={styles.userInfo}>
      <View style={styles.avatar}>
        <Avatar rounded size="medium" source={{uri: event!.creator.avatar}} />
      </View>
      <View style={styles.info}>
        <Text style={styles.username}>{event!.creator.username}</Text>
        <Text style={styles.publishedDate}>{getPublishedDate()}</Text>
      </View>
    </View>
  );
};

export default EventUserInfoSection;

const styles = StyleSheet.create({
  userInfo: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingBottom: 15,
  },
  avatar: {
    marginHorizontal: 5,
  },
  info: {
    justifyContent: 'space-evenly',
    paddingHorizontal: 10,
  },
  username: {
    color: '#67616D',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 15,
  },
  publishedDate: {
    color: '#67616D',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 14,
  },
});
