import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

// Custom Components
import ChannelChip from '../../../../components/ChannelChip';

// Redux
import {useSelector} from 'react-redux';
import {RootState} from '../../../../redux/store';

const EventTitleSection = () => {
  const {event} = useSelector((state: RootState) => state.event);

  return (
    <View style={styles.container}>
      <ChannelChip event={event!} />
      <Text style={styles.title}>{event!.name}</Text>
    </View>
  );
};

export default EventTitleSection;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 15,
  },
  title: {
    padding: 10,
    color: '#453257',
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 20,
  },
});
