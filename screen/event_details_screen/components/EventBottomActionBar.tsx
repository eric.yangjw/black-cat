import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

// Custom Components
import {
  CheckLogo,
  CheckOutlineLogo,
  LikeLogo,
  LikeOutlineLogo,
  SingleCommentLogo,
} from '../../../common/SVG';

// Redux
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../redux/store';
import {
  addGoingToEventAction,
  addLikeToEventAction,
  removeGoingFromEventAction,
  removeLikeFromEventAction,
} from '../redux/eventActions';

interface ActionProps {
  toggleCommenting: () => void;
}

const EventBottomActionBar = ({toggleCommenting}: ActionProps) => {
  const {event} = useSelector((state: RootState) => state.event);
  const authUser = useSelector((state: RootState) => state.auth.authUser);

  const isGoing =
    event!.goings.filter(going => going.id === authUser!.id).length > 0;
  const hasLiked =
    event!.likes.filter(likes => likes.id === authUser!.id).length > 0;

  const dispatch = useDispatch();

  const toggleLikeStatus = () => {
    hasLiked
      ? dispatch(removeLikeFromEventAction(event!.id, authUser!.id))
      : dispatch(addLikeToEventAction(event!.id));
  };

  const toggleIsGoingStatus = () => {
    isGoing
      ? dispatch(removeGoingFromEventAction(event!.id, authUser!.id))
      : dispatch(addGoingToEventAction(event!.id));
  };

  return (
    <View style={styles.container}>
      <View style={styles.leftSection}>
        <TouchableOpacity style={styles.button} onPress={toggleCommenting}>
          <SingleCommentLogo height={25} width={25} />
        </TouchableOpacity>
        <View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => toggleLikeStatus()}>
            {hasLiked ? (
              <LikeLogo color={'#D5EF7F'} height={25} width={25} />
            ) : (
              <LikeOutlineLogo color={'#49355C'} height={25} width={25} />
            )}
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.rightSection}>
        <TouchableOpacity
          style={styles.joinButton}
          onPress={() => toggleIsGoingStatus()}>
          {isGoing ? (
            <CheckLogo color={'#8560A9'} height={25} width={25} />
          ) : (
            <CheckOutlineLogo color={'#788C36'} height={25} width={25} />
          )}
          <Text style={styles.joinText}>
            {isGoing ? 'I am going!' : 'Join'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default EventBottomActionBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    margin: 0,
    height: 60,
  },
  leftSection: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    backgroundColor: '#8560A9',
  },
  button: {
    height: '100%',
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  joinButton: {
    backgroundColor: '#D5EF7F',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  joinText: {
    color: '#788C36',
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 14,
  },
  rightSection: {
    flex: 1,
    height: '100%',
  },
});
