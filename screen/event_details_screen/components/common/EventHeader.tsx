import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

interface HeaderProps {
  title: string;
}

const EventHeader = ({title}: HeaderProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>{title}</Text>
    </View>
  );
};

export default EventHeader;

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    marginBottom: 10,
    marginHorizontal: 15,
    paddingLeft: 5,
    borderLeftWidth: 3,
    borderLeftColor: '#8560A9',
  },
  headerText: {
    color: '#8560A9',
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 16,
  },
});
