import React, {RefObject, useCallback, useRef, useState} from 'react';
import {StyleSheet, View, Image, FlatList} from 'react-native';
import {Divider} from 'react-native-elements';

// Custom Components
import EventHeader from './common/EventHeader';
import EventActionTabRow from './event_content/EventActionTabRow';
import EventDateSection from './event_content/EventDateSection';
import EventDescriptionSection from './event_content/EventDescriptionSection';
import EventGoingSection from './event_content/EventGoingSection';
import EventLikeSection from './event_content/EventLikeSection';
import EventLocationSection from './event_content/EventLocationSection';
import EventTitleSection from './event_content/EventTitleSection';
import EventUserInfoSection from './event_content/EventUserInfoSection';

// Enum
import {EventContentType} from '../../../common/enum/contentType';

// Redux
import {RootState} from '../../../redux/store';
import {useSelector} from 'react-redux';
import EventComment from './event_content/EventComment';
import {Comment} from '../../../model/comment';

enum DetailElementType {
  TITLE = 'TITLE',
  USER_INFO = 'USER_INFO',
  ACTION_TAB = 'ACTION_TAB',
  IMAGE = 'IMAGE',
  DESCRIPTION = 'DESCRIPTION',
  DIVIDER = 'DIVIDER',
  HEADER = 'HEADER',
  DATE = 'DATE',
  LOCATION = 'LOCATION',
  GOING = 'GOING',
  LIKE = 'LIKE',
  COMMENT = 'COMMENT',
}

interface DetailContentProps {
  contentRef: RefObject<FlatList<any>>;
  setComment: (name: string) => void;
  setIsCommenting: (isCommenting: boolean) => void;
}

const EventDetailContent = ({
  contentRef,
  setComment,
  setIsCommenting,
}: DetailContentProps) => {
  const {event} = useSelector((state: RootState) => state.event);
  const comments = useSelector((state: RootState) => state.event.comments);
  const [contentType, setContentType] = useState(EventContentType.Details);

  const eventDetailOrder = () => {
    const list = comments!.map((comment, idx) => ({
      type: DetailElementType.COMMENT,
      comment,
      key: 'comment-' + idx.toString(),
    }));

    return [
      {type: DetailElementType.TITLE},
      {type: DetailElementType.USER_INFO},
      {type: DetailElementType.ACTION_TAB},
      {type: DetailElementType.IMAGE},
      {type: DetailElementType.DESCRIPTION},
      {type: DetailElementType.DIVIDER},
      {type: DetailElementType.HEADER, title: 'When'},
      {type: DetailElementType.DATE},
      {type: DetailElementType.DIVIDER},
      {type: DetailElementType.HEADER, title: 'Where'},
      {type: DetailElementType.LOCATION},
      {type: DetailElementType.GOING},
      {type: DetailElementType.DIVIDER},
      {type: DetailElementType.LIKE},
      {type: DetailElementType.DIVIDER},
      ...list,
    ];
  };

  const renderDetails = (item: {
    type: DetailElementType;
    title?: string;
    comment?: Comment;
    key?: string;
  }) => {
    switch (item.type) {
      case DetailElementType.TITLE:
        return <EventTitleSection />;
      case DetailElementType.USER_INFO:
        return <EventUserInfoSection />;
      case DetailElementType.ACTION_TAB:
        return (
          <EventActionTabRow
            currentContent={contentType}
            toggleTab={(tab: EventContentType): void => jumpTo(tab)}
          />
        );
      case DetailElementType.IMAGE:
        return event!.images.length > 0 ? (
          <View style={styles.imageSection}>
            <FlatList
              horizontal={true}
              data={event!.images}
              renderItem={renderImages}
              keyExtractor={(item, idx) => idx.toString()}
            />
          </View>
        ) : null;
      case DetailElementType.DESCRIPTION:
        return <EventDescriptionSection />;
      case DetailElementType.HEADER:
        return <EventHeader title={item.title!} />;
      case DetailElementType.DIVIDER:
        return <Divider />;
      case DetailElementType.DATE:
        return <EventDateSection />;
      case DetailElementType.LOCATION:
        return <EventLocationSection />;
      case DetailElementType.GOING:
        return <EventGoingSection />;
      case DetailElementType.LIKE:
        return <EventLikeSection />;
      case DetailElementType.COMMENT:
        return (
          <EventComment
            key={item.key}
            comment={item.comment!}
            replyToUser={username => {
              setComment('@' + username + ' ');
              setIsCommenting(true);
            }}
          />
        );
      default:
        return <View />;
    }
  };

  const renderImages = useCallback(({item}: {item: string}) => {
    return (
      <View style={styles.image}>
        <Image
          style={{width: 150, height: 100, paddingHorizontal: 10}}
          source={{uri: item}}></Image>
      </View>
    );
  }, []);

  const jumpTo = (tab: EventContentType) => {
    switch (tab) {
      case EventContentType.Details:
        contentRef.current?.scrollToIndex({
          animated: true,
          index: 3,
          viewOffset: 45,
        });
        break;

      case EventContentType.Participants:
        contentRef.current?.scrollToIndex({
          animated: true,
          index: 11,
          viewOffset: 45,
        });
        break;

      case EventContentType.Comments:
        contentRef.current?.scrollToIndex({
          animated: true,
          index: 15,
          viewOffset: 45,
        });
        break;

      default:
        contentRef.current?.scrollToIndex({
          animated: true,
          index: 0,
          viewOffset: 0,
        });
        break;
    }
  };

  const toggleTabHighlight = useCallback(({viewableItems}) => {
    if (viewableItems[0] !== undefined) {
      if (viewableItems[0].index === 14) {
        setContentType(EventContentType.Comments);
      } else if (viewableItems[0].index === 11) {
        setContentType(EventContentType.Participants);
      } else if (viewableItems[0].index <= 5) {
        setContentType(EventContentType.Details);
      }
    }
  }, []);

  const viewConfig = useRef({itemVisiblePercentThreshold: 100});

  return (
    <View style={styles.container}>
      <FlatList
        ref={contentRef}
        data={eventDetailOrder()}
        renderItem={({item}) => renderDetails(item)}
        stickyHeaderIndices={[2]}
        keyExtractor={(item, index) => index.toString()}
        viewabilityConfig={viewConfig.current}
        onViewableItemsChanged={toggleTabHighlight}
      />
    </View>
  );
};
export default EventDetailContent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  imageSection: {
    padding: 10,
    width: '100%',
  },
  image: {
    marginHorizontal: 10,
  },
});
