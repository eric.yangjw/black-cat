import React, {RefObject} from 'react';
import {StyleSheet, View, TextInput, FlatList} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

// Custom Components
import {CrossLogo, SendLogo} from '../../../common/SVG';

// Redux
import {useDispatch, useSelector} from 'react-redux';
import {addComment} from '../redux/eventActions';
import {RootState} from '../../../redux/store';

interface PropsType {
  contentRef: RefObject<FlatList<any>>;
  comment: string;
  setComment: (comment: string) => void;
  toggleCommenting: () => void;
  displayToast: () => void;
  setToastMessage: (message: string) => void;
}

const EventBottomCommentBar = ({
  contentRef,
  comment,
  setComment,
  toggleCommenting,
  displayToast,
  setToastMessage,
}: PropsType) => {
  const {event} = useSelector((state: RootState) => state.event);

  const dispatch = useDispatch();

  const onChangeText = (input: string) => {
    setComment(input);
  };

  const sendComment = () => {
    if (comment === '') {
      setToastMessage(`Please enter a message`);
      displayToast();
    } else {
      dispatch(addComment(event!.id, comment));
      setToastMessage(`Message sent! Event: ${event!.id}`);
      setComment('');
      toggleCommenting();
      displayToast();
      setTimeout(() => contentRef.current?.scrollToEnd(), 800);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.cancelButtonSection}>
        <TouchableOpacity
          style={styles.cancelButton}
          onPress={() => toggleCommenting()}>
          <CrossLogo color={'#D5EF7F'} height={22} width={22} />
        </TouchableOpacity>
      </View>
      <View style={styles.commentInputBlock}>
        <TextInput
          style={styles.commentInput}
          placeholder={'Leave your comment here...'}
          placeholderTextColor="#E8E8E8"
          value={comment}
          onChange={event => onChangeText(event.nativeEvent.text)}
        />
      </View>
      <View style={styles.sendButtonSection}>
        <TouchableOpacity
          style={styles.sendButton}
          onPress={() => sendComment()}>
          <SendLogo height={25} width={25} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default EventBottomCommentBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    margin: 0,
    height: 60,
    backgroundColor: '#8560A9',
  },
  cancelButtonSection: {width: 65, padding: 10},
  cancelButton: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  commentInputBlock: {
    padding: 7,
    paddingVertical: 10,
    flex: 1,
  },
  sendButtonSection: {
    width: 70,
    padding: 10,
    backgroundColor: '#D5EF7F',
  },
  sendButton: {
    // borderRadius: 5,
    // borderWidth: 1,
    // borderColor: '#453257',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  commentInput: {
    backgroundColor: '#FFFFFF',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 12,
    color: '#453257',
    height: '100%',
    paddingHorizontal: 15,
    marginHorizontal: 5,
    borderRadius: 25,
  },
});
