import {AppEvent} from '../../../model/appEvent';
import {Comment} from '../../../model/comment';
import {EventAction, EventActionType} from './eventActions';

interface EventState {
  eventId: number | null;
  event: AppEvent | null;
  hasLiked: boolean;
  isGoing: boolean;
  comments: Comment[] | null;
}

const eventReducer = (
  prevState: EventState = {
    eventId: null,
    event: null,
    hasLiked: false,
    isGoing: false,
    comments: null,
  },
  action: EventAction,
): EventState => {
  switch (action.type) {
    case EventActionType.SET_EVENT:
      return {
        eventId: action.eventId,
        event: action.event,
        hasLiked: action.hasLiked,
        isGoing: action.isGoing,
        comments: action.comments,
      };

    case EventActionType.ADD_LIKE:
      return {
        ...prevState,
        event: action.event,
        hasLiked: action.hasLiked,
      };

    case EventActionType.ADD_GOING:
      return {
        ...prevState,
        event: action.event,
        isGoing: action.isGoing,
      };
    case EventActionType.REMOVE_LIKE:
      return {
        ...prevState,
        event: action.event,
        hasLiked: action.hasLiked,
      };
    case EventActionType.REMOVE_GOING:
      return {
        ...prevState,
        event: action.event,
        isGoing: action.isGoing,
      };
    case EventActionType.ADD_COMMENT:
      return {
        ...prevState,
        comments: [...prevState.comments!, action.comment],
      };

    default:
      return {...prevState};
  }
};

export default eventReducer;
