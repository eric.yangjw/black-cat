import {AxiosResponse} from 'axios';
import {AppEvent, AppEventJson} from '../../../model/appEvent';
import {Comment, CommentJson} from '../../../model/comment';
import {
  addCommentToEventInServer,
  addGoingToEventInServer,
  addLikeToEventInServer,
  removeGoingFromEventInServer,
  removeLikeFromEventInServer,
} from '../../../service/eventService';

export enum EventActionType {
  SET_EVENT = 'SET_EVENT',
  ADD_LIKE = 'ADD_LIKE',
  ADD_GOING = 'ADD_GOING',
  REMOVE_LIKE = 'REMOVE_LIKE',
  REMOVE_GOING = 'REMOVE_GOING',
  ADD_COMMENT = 'ADD_COMMENT',
}

export type EventAction =
  | {
      type: EventActionType.SET_EVENT;
      eventId: number;
      event: AppEvent;
      hasLiked: boolean;
      isGoing: boolean;
      comments: Comment[];
    }
  | {
      type: EventActionType.ADD_LIKE;
      event: AppEvent;
      hasLiked: boolean;
    }
  | {
      type: EventActionType.ADD_GOING;
      event: AppEvent;
      isGoing: boolean;
    }
  | {
      type: EventActionType.REMOVE_LIKE;
      event: AppEvent;
      hasLiked: boolean;
    }
  | {
      type: EventActionType.REMOVE_GOING;
      event: AppEvent;
      isGoing: boolean;
    }
  | {type: EventActionType.ADD_COMMENT; comment: Comment};

export const setEventAction = (
  event: AppEvent,
  userId: number,
): EventAction => {
  const hasLiked = event.likes.filter(like => like.id === userId).length > 0;
  const isGoing = event.goings.filter(going => going.id === userId).length > 0;
  const comments = event.comments;
  const eventId = event.id;

  return {
    type: EventActionType.SET_EVENT,
    eventId,
    event,
    hasLiked,
    isGoing,
    comments,
  };
};

export const addLikeToEventAction =
  (eventId: number) =>
  (dispatch: (action: EventAction) => void, getState: any) => {
    addLikeToEventInServer(eventId)
      .then((res: AxiosResponse) => {
        const {event} = res.data;
        dispatch(_addLikeAction(event));
      })
      .catch((e: TypeError) => {
        console.log(`### addLikeToEventAction statusCode: ${e.message}`);
      });
  };

const _addLikeAction = (event: AppEventJson): EventAction => {
  return {
    type: EventActionType.ADD_LIKE,
    event: AppEvent.fromData(event),
    hasLiked: true,
  };
};

export const removeLikeFromEventAction =
  (eventId: number, userId: number) =>
  (dispatch: (action: EventAction) => void, getState: any) => {
    removeLikeFromEventInServer(eventId, userId)
      .then((res: AxiosResponse) => {
        const {event} = res.data;
        dispatch(_removeLikeAction(event));
      })
      .catch((e: TypeError) => {
        console.log(`### removeLikeFromEventAction statusCode: ${e.message}`);
      });
  };

const _removeLikeAction = (event: AppEventJson): EventAction => {
  return {
    type: EventActionType.REMOVE_LIKE,
    event: AppEvent.fromData(event),
    hasLiked: false,
  };
};

export const addGoingToEventAction =
  (eventId: number) =>
  (dispatch: (action: EventAction) => void, getState: any) => {
    addGoingToEventInServer(eventId)
      .then((res: AxiosResponse) => {
        const {event} = res.data;
        dispatch(_addGoingAction(event));
      })
      .catch((e: TypeError) => {
        console.log(`### addGoingToEventAction statusCode: ${e.message}`);
      });
  };

const _addGoingAction = (event: AppEventJson): EventAction => {
  return {
    type: EventActionType.ADD_GOING,
    event: AppEvent.fromData(event),
    isGoing: true,
  };
};

export const removeGoingFromEventAction =
  (eventId: number, userId: number) =>
  (dispatch: (action: EventAction) => void, getState: any) => {
    removeGoingFromEventInServer(eventId, userId)
      .then((res: AxiosResponse) => {
        const {event} = res.data;
        dispatch(_removeGoingAction(event));
      })
      .catch((e: TypeError) => {
        console.log(`### removeGoingFromEventAction statusCode: ${e.message}`);
      });
  };

const _removeGoingAction = (event: AppEventJson): EventAction => {
  return {
    type: EventActionType.REMOVE_GOING,
    event: AppEvent.fromData(event),
    isGoing: false,
  };
};

export const addComment =
  (eventId: number, comment: string) =>
  (dispatch: (action: EventAction) => void, getState: any) => {
    addCommentToEventInServer(eventId, comment)
      .then((res: AxiosResponse) => {
        const {comment} = res.data;
        dispatch(_addNewCommentAction(comment));
      })
      .catch((e: TypeError) => {
        console.log(`### addComment statusCode: ${e.message}`);
      });
  };

const _addNewCommentAction = (comment: CommentJson): EventAction => {
  return {
    type: EventActionType.ADD_COMMENT,
    comment: Comment.fromData(comment),
  };
};
