import React, {useEffect, useRef, useState} from 'react';
import {ActivityIndicator, FlatList, StyleSheet, View} from 'react-native';

// Custom Components
import EmptyFeed from '../../components/EmptyFeed';
import AppHeader from '../../components/app_header/AppHeader';
import EventCard from '../../components/event_card/EventCard';

// Redux
import {RootState} from '../../redux/store';
import {useDispatch, useSelector} from 'react-redux';
import {
  retrieveAllEventsAction,
  retrieveMoreEventsAction,
  retrieveMoreFilteredEventsAction,
} from './redux/feedActions';

// Model
import {AppEvent} from '../../model/appEvent';
import FilterHeader from '../../components/FilterHeader';

const FeedScreen = () => {
  const dispatch = useDispatch();
  const ref = useRef<FlatList<any>>(null);
  const [listPosition, setListPosition] = useState(0);
  const {event} = useSelector((state: RootState) => state.event);
  const {
    eventsList,
    filteredList,
    criteria,
    hasMore,
    hasMoreFiltered,
    channel,
    before,
    after,
    hasLoaded,
  } = useSelector((state: RootState) => state.feed);

  useEffect(() => {
    dispatch(retrieveAllEventsAction());
  }, [event]);

  const renderEvent = (item: AppEvent, index: number) => (
    <EventCard key={item.id + index} event={item} />
  );

  const fetchMoreData = () => {
    console.log('Fetch more data');
    if (hasLoaded) {
      if (filteredList === null) {
        dispatch(retrieveMoreEventsAction(eventsList!.length));
      } else {
        dispatch(
          retrieveMoreFilteredEventsAction({
            offset: filteredList.length,
            channel,
            before,
            after,
          }),
        );
      }
    }
  };

  const hasList = () => {
    if (filteredList === null) {
      return eventsList != null && eventsList.length > 0;
    } else {
      return filteredList.length > 0;
    }
  };

  if (!hasLoaded) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
        }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  const clearFilter = () => {
    ref.current?.scrollToOffset({animated: false, offset: listPosition});
  };

  return (
    <View style={styles.container}>
      <AppHeader isFeed={true} />
      {criteria !== '' ? (
        <FilterHeader clearFilter={clearFilter} />
      ) : (
        <View></View>
      )}
      {hasList() ? (
        <FlatList
          ref={ref}
          style={{padding: 10, backgroundColor: 'white'}}
          data={filteredList === null ? eventsList : filteredList}
          renderItem={({item, index}) => renderEvent(item, index)}
          keyExtractor={(item, idx) => item.id.toString()}
          onEndReached={fetchMoreData}
          onEndReachedThreshold={0.01}
          onScroll={e => setListPosition(e.nativeEvent.contentOffset.y)}
          scrollEventThrottle={200}
          ListFooterComponent={() => {
            const condition = filteredList === null ? hasMore : hasMoreFiltered;
            if (condition) {
              return (
                <View
                  style={{
                    paddingBottom: 25,
                    paddingTop: 10,
                    flex: 1,
                    justifyContent: 'center',
                  }}>
                  <ActivityIndicator size="large" />
                </View>
              );
            } else {
              return <></>;
            }
          }}
        />
      ) : (
        <EmptyFeed />
      )}
    </View>
  );
};

export default FeedScreen;

const styles = StyleSheet.create({
  container: {height: '100%'},
  branding: {flex: 5, justifyContent: 'center'},
  form: {
    flex: 3,
    alignItems: 'center',
    height: '100%',
    width: '100%',
    justifyContent: 'space-between',
  },

  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
