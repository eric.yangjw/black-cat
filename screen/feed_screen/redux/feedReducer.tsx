import {AppEvent} from '../../../model/appEvent';
import {FeedAction, FeedActionType} from './feedActions';

export interface FeedState {
  eventsList: AppEvent[] | null;
  filteredList: AppEvent[] | null;
  criteria: string;
  hasMore: boolean;
  hasMoreFiltered: boolean;
  channel: number;
  before: number;
  after: number;
  hasLoaded: boolean;
}

const feedReducer = (
  prevState: FeedState = {
    eventsList: null,
    filteredList: null,
    hasMore: false,
    hasMoreFiltered: false,
    criteria: '',
    channel: -1,
    before: -1,
    after: -1,
    hasLoaded: true,
  },
  action: FeedAction,
): FeedState => {
  switch (action.type) {
    case FeedActionType.IS_LOADING:
      return {...prevState, hasLoaded: action.hasLoaded};

    case FeedActionType.HAS_LOADED:
      return {...prevState, hasLoaded: action.hasLoaded};
    case FeedActionType.SET_ALL_EVENTS:
      return {
        ...prevState,
        eventsList: action.events,
        hasMore: action.hasMore,
      };
    case FeedActionType.ADD_MORE_EVENTS:
      return {
        ...prevState,
        eventsList: [...prevState.eventsList!, ...action.events],
        hasMore: action.hasMore,
      };
    case FeedActionType.ADD_MORE_FILTERED_EVENTS:
      return {
        ...prevState,
        filteredList: [...prevState.filteredList!, ...action.filtered],
        hasMore: action.hasMore,
      };
    case FeedActionType.SET_FILTERED_EVENTS:
      return {
        ...prevState,
        filteredList: action.filtered,
        criteria: action.criteria,
        hasMoreFiltered: action.hasMore,
        channel: action.channel,
        before: action.before,
        after: action.after,
      };

    case FeedActionType.CLEAR_FILTERED_EVENTS:
      return {
        ...prevState,
        filteredList: null,
        criteria: '',
        hasMoreFiltered: false,
        channel: -1,
        before: -1,
        after: -1,
      };

    default:
      return {...prevState};
  }
};

export default feedReducer;
