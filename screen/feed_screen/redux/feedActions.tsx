import {AxiosResponse} from 'axios';
import {AppEvent, AppEventJson} from '../../../model/appEvent';
import {
  fetchFeedFromServer,
  fetchFilteredEventsFromSever,
  FilteredEventParams,
} from '../../../service/feedService';

interface FeedRes {
  events: AppEventJson[];
  has_more: boolean;
}

export enum FeedActionType {
  SET_ALL_EVENTS = 'SET_ALL_EVENTS',
  ADD_MORE_EVENTS = 'ADD_MORE_EVENTS',
  ADD_MORE_FILTERED_EVENTS = 'ADD_MORE_FILTERED_EVENTS',
  SET_FILTERED_EVENTS = 'SET_FILTERED_EVENTS',
  CLEAR_FILTERED_EVENTS = 'CLEAR_FILTERED_EVENTS',
  IS_LOADING = 'IS_LOADING',
  HAS_LOADED = 'HAS_LOADED',
}

export type FeedAction =
  | {
      type: FeedActionType.SET_ALL_EVENTS;
      events: AppEvent[];
      hasMore: boolean;
    }
  | {
      type: FeedActionType.ADD_MORE_EVENTS;
      events: AppEvent[];
      hasMore: boolean;
    }
  | {
      type: FeedActionType.ADD_MORE_FILTERED_EVENTS;
      filtered: AppEvent[];
      hasMore: boolean;
    }
  | {
      type: FeedActionType.SET_FILTERED_EVENTS;
      filtered: AppEvent[];
      hasMore: boolean;
      criteria: string;
      channel: number;
      before: number;
      after: number;
    }
  | {
      type: FeedActionType.CLEAR_FILTERED_EVENTS;
    }
  | {
      type: FeedActionType.HAS_LOADED;
      hasLoaded: boolean;
    }
  | {
      type: FeedActionType.IS_LOADING;
      hasLoaded: boolean;
    };

export const retrieveAllEventsAction =
  () =>
  (dispatch: (action: FeedAction) => void, getState: any): void => {
    fetchFeedFromServer()
      .then((res: AxiosResponse) => {
        const {events, has_more}: FeedRes = res.data;
        return dispatch(_setAllEventsAction(events, has_more));
      })
      .catch((e: TypeError) =>
        console.log('### Error in getFeedFromServer: ', e.message),
      );
  };

export const retrieveMoreEventsAction =
  (offset: number) =>
  (dispatch: (action: FeedAction) => void, getState: any) => {
    dispatch(_setLoadingAction());
    fetchFilteredEventsFromSever({
      offset,
    })
      .then((res: AxiosResponse) => {
        const {events, has_more} = res.data;
        dispatch(_addMoreEventsAction(events, has_more));
      })
      .catch((e: TypeError) =>
        console.log(`### retrieveFilteredEvents statusCode: ${e.message}`),
      );

    dispatch(_setLoadedAction());
  };

export const retrieveMoreFilteredEventsAction =
  ({offset, channel, before, after}: FilteredEventParams) =>
  (dispatch: (action: FeedAction) => void, getState: any) => {
    fetchFilteredEventsFromSever({
      offset,
      channel,
      before,
      after,
    })
      .then((res: AxiosResponse) => {
        const {events, has_more} = res.data;
        dispatch(_addMoreFilteredEventsAction(events, has_more));
      })
      .catch((e: TypeError) =>
        console.log(`### retrieveFilteredEvents statusCode: ${e.message}`),
      );
  };

export const retrieveFilteredEventsAction =
  ({offset, channel, before, after}: FilteredEventParams, criteria: string) =>
  async (dispatch: (action: FeedAction) => void, getState: any) => {
    dispatch(_setLoadingAction());

    let hasMore = true;
    let off = offset ?? 0;
    let list: AppEventJson[] = [];

    while (hasMore) {
      const res = await fetchFilteredEventsFromSever({
        offset: off,
        channel,
        before,
        after,
      });
      const {events, has_more} = res.data;
      list = [...list, ...events];
      hasMore = has_more;
      off = list.length;
    }

    dispatch(
      _setFilteredEventsAction(
        list,
        hasMore,
        criteria,
        channel ?? -1,
        before ?? -1,
        after ?? -1,
      ),
    );
    dispatch(_setLoadedAction());
  };

export const clearFilteredEventsAction = (): FeedAction => {
  return {
    type: FeedActionType.CLEAR_FILTERED_EVENTS,
  };
};

const _setAllEventsAction = (
  events: AppEventJson[],
  hasMore: boolean,
): FeedAction => {
  return {
    type: FeedActionType.SET_ALL_EVENTS,
    events: AppEvent.listFromData(events),
    hasMore,
  };
};

const _addMoreEventsAction = (
  events: AppEventJson[],
  hasMore: boolean,
): FeedAction => {
  return {
    type: FeedActionType.ADD_MORE_EVENTS,
    events: AppEvent.listFromData(events),
    hasMore,
  };
};

const _addMoreFilteredEventsAction = (
  events: AppEventJson[],
  hasMore: boolean,
): FeedAction => {
  return {
    type: FeedActionType.ADD_MORE_FILTERED_EVENTS,
    filtered: AppEvent.listFromData(events),
    hasMore,
  };
};

const _setFilteredEventsAction = (
  events: AppEventJson[],
  hasMore: boolean,
  criteria: string,
  channel: number,
  before: number,
  after: number,
): FeedAction => {
  return {
    type: FeedActionType.SET_FILTERED_EVENTS,
    filtered: AppEvent.listFromData(events),
    hasMore,
    criteria,
    channel,
    before,
    after,
  };
};

const _setLoadingAction = (): FeedAction => {
  return {
    type: FeedActionType.IS_LOADING,
    hasLoaded: false,
  };
};

const _setLoadedAction = (): FeedAction => {
  return {
    type: FeedActionType.HAS_LOADED,
    hasLoaded: true,
  };
};
