import React from 'react';
import {StyleProp, StyleSheet, Text, View, ViewStyle} from 'react-native';
import {Avatar} from 'react-native-elements/dist/avatar/Avatar';
import {useSelector} from 'react-redux';
import {EmailLogo} from '../../../common/SVG';
import {RootState} from '../../../redux/store';

interface UserInfoProps {
  style: StyleProp<ViewStyle>;
}

const UserInfo = (props: UserInfoProps) => {
  const {avatar, username, email} = useSelector(
    (state: RootState) => state.auth.authUser!,
  );

  return (
    <View style={[props.style, styles.container]}>
      <Avatar
        containerStyle={styles.avatar}
        rounded
        size={120}
        source={{uri: avatar}}
      />
      <Text style={styles.username}>{username}</Text>
      <View style={styles.emailRow}>
        <EmailLogo />
        <Text style={styles.email}>{email}</Text>
      </View>
    </View>
  );
};

export default UserInfo;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
    backgroundColor: 'white',
  },
  avatar: {
    borderWidth: 5,
    borderColor: '#D3C1E5',
  },
  username: {
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 24,
    color: '#67616D',
  },
  emailRow: {flexDirection: 'row', alignItems: 'center'},
  email: {
    paddingHorizontal: 10,
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 14,
    color: '#453257',
  },
});
