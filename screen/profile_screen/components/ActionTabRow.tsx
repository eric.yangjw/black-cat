import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

// Custom components
import {
  CheckLogo,
  CheckOutlineLogo,
  LikeLogo,
  LikeOutlineLogo,
  PastLogo,
  PastOutlineLogo,
} from '../../../common/SVG';

// Redux
import {RootState} from '../../../redux/store';
import {useSelector} from 'react-redux';

// Enum
import {ProfileContentType} from '../../../common/enum/contentType';

interface ActionTabProps {
  toggleTab: (tab: ProfileContentType) => void;
  currentState: ProfileContentType;
}

const ActionTabRow = ({toggleTab, currentState}: ActionTabProps) => {
  const {liked, going, past} = useSelector((state: RootState) => state.profile);

  const getLikeCountMsg = (): string => {
    if (liked.count <= 1) return `${liked.count} Like`;

    return `${liked.count} Likes`;
  };

  const getGoingCountMsg = () => {
    return `${going.count} Going`;
  };

  const getPastCountMsg = () => {
    return `${past.count} Past`;
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.tab}
        onPress={() => toggleTab(ProfileContentType.Liked)}>
        {currentState === ProfileContentType.Liked ? (
          <LikeLogo />
        ) : (
          <LikeOutlineLogo />
        )}
        <Text>{getLikeCountMsg()}</Text>
      </TouchableOpacity>
      <View style={styles.divider} />
      <TouchableOpacity
        style={styles.tab}
        onPress={() => toggleTab(ProfileContentType.Going)}>
        {currentState === ProfileContentType.Going ? (
          <CheckLogo />
        ) : (
          <CheckOutlineLogo />
        )}
        <Text>{getGoingCountMsg()}</Text>
      </TouchableOpacity>
      <View style={styles.divider} />
      <TouchableOpacity
        style={styles.tab}
        onPress={() => toggleTab(ProfileContentType.Past)}>
        {currentState === ProfileContentType.Past ? (
          <PastLogo />
        ) : (
          <PastOutlineLogo />
        )}
        <Text>{getPastCountMsg()}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ActionTabRow;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#E8E8E8',
    alignItems: 'center',
  },
  tab: {
    height: 45,
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  divider: {height: '70%', width: 1, backgroundColor: '#E8E8E8'},
});
