import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  NativeSyntheticEvent,
  NativeScrollEvent,
  ActivityIndicator,
} from 'react-native';

// Custom Components
import AppHeader from '../../components/app_header/AppHeader';
import UserInfo from './components/UserInfo';
import ActionTabRow from './components/ActionTabRow';
import EmptyFeed from '../../components/EmptyFeed';
import EventCard from '../../components/event_card/EventCard';

// Enum
import {ProfileContentType} from '../../common/enum/contentType';

// Redux
import {RootState} from '../../redux/store';
import {useDispatch, useSelector} from 'react-redux';
import {
  retrieveInitialEventList,
  retrieveMoreEvents,
} from './redux/profileActions';
import {AppEvent} from '../../model/appEvent';

const ProfileScreen = () => {
  const dispatch = useDispatch();

  const [contentType, setContentType] = useState(ProfileContentType.Liked);
  const updateState = (newState: ProfileContentType) =>
    setContentType(newState);

  const {authUser} = useSelector((state: RootState) => state.auth);
  const userId = authUser != null ? authUser.id : -1;
  const {liked, going, past} = useSelector((state: RootState) => state.profile);

  useEffect(() => {
    if (liked.list === null)
      dispatch(retrieveInitialEventList(userId, ProfileContentType.Liked));
    if (going.list === null)
      dispatch(retrieveInitialEventList(userId, ProfileContentType.Going));
    if (past.list === null)
      dispatch(retrieveInitialEventList(userId, ProfileContentType.Past));
  }, []);

  const toggleTab = (newState: ProfileContentType) => {
    updateState(newState);
  };

  enum Section {
    USER_INFO = 'USER_INFO',
    ACTION_TAB = 'ACTION_TAB',
    EMPTY = 'EMPTY',
    EVENT = 'EVENT',
  }

  type SectionType =
    | {type: Section.USER_INFO}
    | {type: Section.ACTION_TAB}
    | {type: Section.EMPTY}
    | {type: Section.EVENT; item: AppEvent; key: string};

  const likeListOrder = () => {
    const list =
      liked.list === null || liked.list.length === 0
        ? [{type: Section.EMPTY}]
        : liked.list.map((item, index) => ({
            type: Section.EVENT,
            item,
            key: index.toString(),
          }));

    return [{type: Section.USER_INFO}, {type: Section.ACTION_TAB}, ...list];
  };

  const goingListOrder = () => {
    const list =
      going.list === null || going.list.length === 0
        ? [{type: Section.EMPTY}]
        : going.list.map((item, index) => ({
            type: Section.EVENT,
            item,
            key: index.toString(),
          }));

    return [{type: Section.USER_INFO}, {type: Section.ACTION_TAB}, ...list];
  };

  const pastListOrder = () => {
    const list =
      past.list === null || past.list.length === 0
        ? [{type: Section.EMPTY}]
        : past.list.map((item, index) => ({
            type: Section.EVENT,
            item,
            key: index.toString(),
          }));

    return [{type: Section.USER_INFO}, {type: Section.ACTION_TAB}, ...list];
  };

  const renderItem = (section: SectionType) => {
    switch (section.type) {
      case Section.USER_INFO:
        return <UserInfo style={styles.userInfo} />;
      case Section.ACTION_TAB:
        return (
          <ActionTabRow toggleTab={toggleTab} currentState={contentType} />
        );
      case Section.EMPTY:
        return <EmptyFeed />;
      case Section.EVENT:
        return <EventCard event={section.item} key={section.key} />;
      default:
        return <View />;
    }
  };

  const [likePosition, setLikePosition] = useState(0);
  const [goingPosition, setGoingPosition] = useState(0);
  const [pastPosition, setPastPosition] = useState(0);

  const handleScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    if (contentType === ProfileContentType.Liked) {
      setLikePosition(event.nativeEvent.contentOffset.y);
      if (event.nativeEvent.contentOffset.y > 198) {
        if (goingPosition <= 198) setGoingPosition(198);
        if (pastPosition <= 198) setPastPosition(198);
      } else {
        setLikePosition(event.nativeEvent.contentOffset.y);
        setGoingPosition(event.nativeEvent.contentOffset.y);
      }
    }

    if (contentType === ProfileContentType.Going) {
      setGoingPosition(event.nativeEvent.contentOffset.y);
      if (event.nativeEvent.contentOffset.y > 198) {
        if (likePosition <= 198) setLikePosition(198);
        if (pastPosition <= 198) setPastPosition(198);
      } else {
        setLikePosition(event.nativeEvent.contentOffset.y);
        setPastPosition(event.nativeEvent.contentOffset.y);
      }
    }

    if (contentType === ProfileContentType.Past) {
      setPastPosition(event.nativeEvent.contentOffset.y);
      if (event.nativeEvent.contentOffset.y > 198) {
        if (likePosition <= 198) setLikePosition(198);
        if (goingPosition <= 198) setGoingPosition(198);
      } else {
        setLikePosition(event.nativeEvent.contentOffset.y);
        setGoingPosition(event.nativeEvent.contentOffset.y);
      }
    }
  };

  const fetchMoreData = () => {
    let offset = 0;
    if (contentType === ProfileContentType.Liked) {
      offset = liked.list!.length;
    } else if (contentType === ProfileContentType.Going) {
      offset = going.list!.length;
    } else if (contentType === ProfileContentType.Past) {
      offset = past.list!.length;
    }

    dispatch(retrieveMoreEvents(userId, contentType, offset));
  };

  const likeFlatlist = (
    <FlatList
      key={'likeFlatList'}
      contentOffset={{x: 0, y: likePosition}}
      onScrollEndDrag={handleScroll}
      onMomentumScrollEnd={handleScroll}
      style={styles.content}
      data={likeListOrder()}
      renderItem={({item}) => renderItem(item as SectionType)}
      keyExtractor={(_, index) => index.toString()}
      stickyHeaderIndices={[1]}
      onEndReached={fetchMoreData}
      onEndReachedThreshold={0.01}
      ListFooterComponent={() => {
        if (liked.hasMore) {
          return (
            <View
              style={{
                paddingBottom: 25,
                paddingTop: 10,
                flex: 1,
                justifyContent: 'center',
              }}>
              <ActivityIndicator size="large" />
            </View>
          );
        } else {
          return <></>;
        }
      }}
    />
  );

  const goingFlatlist = (
    <FlatList
      key={'goingFlatList'}
      contentOffset={{x: 0, y: goingPosition}}
      onScrollEndDrag={handleScroll}
      onMomentumScrollEnd={handleScroll}
      style={styles.content}
      data={goingListOrder()}
      renderItem={({item}) => renderItem(item as SectionType)}
      keyExtractor={(_, index) => index.toString()}
      stickyHeaderIndices={[1]}
      onEndReached={fetchMoreData}
      onEndReachedThreshold={0.01}
      ListFooterComponent={() => {
        if (going.hasMore) {
          return (
            <View
              style={{
                paddingBottom: 25,
                paddingTop: 10,
                flex: 1,
                justifyContent: 'center',
              }}>
              <ActivityIndicator size="large" />
            </View>
          );
        } else {
          return <></>;
        }
      }}
    />
  );

  const pastFlatlist = (
    <FlatList
      key={'pastFlatList'}
      contentOffset={{x: 0, y: pastPosition}}
      onScrollEndDrag={handleScroll}
      onMomentumScrollEnd={handleScroll}
      style={styles.content}
      data={pastListOrder()}
      renderItem={({item}) => renderItem(item as SectionType)}
      keyExtractor={(_, index) => index.toString()}
      stickyHeaderIndices={[1]}
      onEndReached={fetchMoreData}
      onEndReachedThreshold={0.01}
      ListFooterComponent={() => {
        if (past.hasMore) {
          return (
            <View
              style={{
                paddingBottom: 25,
                paddingTop: 10,
                flex: 1,
                justifyContent: 'center',
              }}>
              <ActivityIndicator size="large" />
            </View>
          );
        } else {
          return <></>;
        }
      }}
    />
  );

  const getFlatlist = () => {
    switch (contentType) {
      case ProfileContentType.Liked:
        return likeFlatlist;
      case ProfileContentType.Going:
        return goingFlatlist;
      case ProfileContentType.Past:
        return pastFlatlist;
      default:
        return <View />;
    }
  };

  return !(liked.list !== null && going.list !== null && past.list !== null) ? (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
      }}>
      <ActivityIndicator size="large" />
    </View>
  ) : (
    <View style={styles.container}>
      <AppHeader />
      {getFlatlist()}
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
  },
  userInfo: {
    paddingTop: 20,
    paddingBottom: 10,
  },
  content: {
    width: '100%',
    backgroundColor: 'white',
  },
});
