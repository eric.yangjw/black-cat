import {AxiosResponse} from 'axios';
import {ProfileContentType} from '../../../common/enum/contentType';
import {AppEvent, AppEventJson} from '../../../model/appEvent';
import {
  fetchAuthUserEventListFromServer,
  fetchTotalEventCountFromServer,
} from '../../../service/profileService';

export enum ProfileActionType {
  SET_USER_LIKED = 'SET_USER_LIKED',
  SET_USER_GOING = 'SET_USER_GOING',
  SET_USER_PAST = 'SET_USER_PAST',
  ADD_USER_LIKED = 'ADD_USER_LIKED',
  ADD_USER_GOING = 'ADD_USER_GOING',
  ADD_USER_PAST = 'ADD_USER_PAST',
}

export type ProfileAction =
  | {
      type: ProfileActionType.SET_USER_LIKED;
      liked: {list: AppEvent[]; hasMore: boolean; count: number};
    }
  | {
      type: ProfileActionType.SET_USER_GOING;
      going: {list: AppEvent[]; hasMore: boolean; count: number};
    }
  | {
      type: ProfileActionType.SET_USER_PAST;
      past: {list: AppEvent[]; hasMore: boolean; count: number};
    }
  | {
      type: ProfileActionType.ADD_USER_LIKED;
      liked: {list: AppEvent[]; hasMore: boolean};
    }
  | {
      type: ProfileActionType.ADD_USER_GOING;
      going: {list: AppEvent[]; hasMore: boolean};
    }
  | {
      type: ProfileActionType.ADD_USER_PAST;
      past: {list: AppEvent[]; hasMore: boolean};
    };

export const retrieveInitialEventList =
  (userId: number, type: ProfileContentType) =>
  (dispatch: (action: ProfileAction) => void, getState: any) => {
    fetchAuthUserEventListFromServer(userId, type, 0)
      .then(async (res: AxiosResponse) => {
        const {
          events,
          has_more: hasMore,
        }: {
          events: AppEventJson[];
          has_more: boolean;
        } = res.data;

        let count = events.length;
        if (hasMore) {
          count = await fetchTotalEventCountFromServer(
            userId,
            type,
            events.length,
          );
        }

        switch (type) {
          case ProfileContentType.Liked:
            dispatch(
              _setUserLikedAction(
                AppEvent.listFromData(events),
                hasMore,
                count,
              ),
            );
            break;
          case ProfileContentType.Going:
            dispatch(
              _setUserGoingAction(
                AppEvent.listFromData(events),
                hasMore,
                count,
              ),
            );
            break;
          case ProfileContentType.Past:
            dispatch(
              _setUserPastAction(AppEvent.listFromData(events), hasMore, count),
            );
            break;
          default:
            throw Error(`Invalid type given: ${type}`);
        }
      })
      .catch((e: TypeError) =>
        console.log(`### retrieveEventList statusCode: ${e.message}`),
      );
  };

export const retrieveMoreEvents =
  (userId: number, type: ProfileContentType, offset: number) =>
  (dispatch: (action: ProfileAction) => void, getState: any) => {
    fetchAuthUserEventListFromServer(userId, type, offset)
      .then(async (res: AxiosResponse) => {
        const {
          events,
          has_more: hasMore,
        }: {
          events: AppEventJson[];
          has_more: boolean;
        } = res.data;

        switch (type) {
          case ProfileContentType.Liked:
            dispatch(
              _addUserLikedEventsAction(AppEvent.listFromData(events), hasMore),
            );
            break;
          case ProfileContentType.Going:
            dispatch(
              _addUserGoingEventsAction(AppEvent.listFromData(events), hasMore),
            );
            break;
          case ProfileContentType.Past:
            dispatch(
              _addUserPastEventsAction(AppEvent.listFromData(events), hasMore),
            );
            break;
          default:
            throw Error(`Invalid type given: ${type}`);
        }
      })
      .catch((e: TypeError) =>
        console.log(`### retrieveEventList statusCode: ${e.message}`),
      );
  };

const _setUserLikedAction = (
  eventsList: AppEvent[],
  hasMore: boolean,
  count: number,
): ProfileAction => {
  return {
    type: ProfileActionType.SET_USER_LIKED,
    liked: {
      list: eventsList,
      hasMore,
      count,
    },
  };
};

const _setUserGoingAction = (
  eventsList: AppEvent[],
  hasMore: boolean,
  count: number,
): ProfileAction => {
  return {
    type: ProfileActionType.SET_USER_GOING,
    going: {
      list: eventsList,
      hasMore,
      count,
    },
  };
};

const _setUserPastAction = (
  eventsList: AppEvent[],
  hasMore: boolean,
  count: number,
): ProfileAction => {
  return {
    type: ProfileActionType.SET_USER_PAST,
    past: {
      list: eventsList,
      hasMore,
      count,
    },
  };
};

const _addUserLikedEventsAction = (
  eventsList: AppEvent[],
  hasMore: boolean,
): ProfileAction => {
  return {
    type: ProfileActionType.ADD_USER_LIKED,
    liked: {
      list: eventsList,
      hasMore,
    },
  };
};
const _addUserGoingEventsAction = (
  eventsList: AppEvent[],
  hasMore: boolean,
): ProfileAction => {
  return {
    type: ProfileActionType.ADD_USER_GOING,
    going: {
      list: eventsList,
      hasMore,
    },
  };
};
const _addUserPastEventsAction = (
  eventsList: AppEvent[],
  hasMore: boolean,
): ProfileAction => {
  return {
    type: ProfileActionType.ADD_USER_PAST,
    past: {
      list: eventsList,
      hasMore,
    },
  };
};
