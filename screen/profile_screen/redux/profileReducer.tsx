import {AppEvent} from '../../../model/appEvent';
import {ProfileAction, ProfileActionType} from './profileActions';

interface ProfileState {
  liked: {list: AppEvent[] | null; hasMore: boolean; count: number};
  going: {list: AppEvent[] | null; hasMore: boolean; count: number};
  past: {list: AppEvent[] | null; hasMore: boolean; count: number};
}

const profileReducer = (
  prevState: ProfileState = {
    liked: {list: null, hasMore: false, count: 0},
    going: {list: null, hasMore: false, count: 0},
    past: {list: null, hasMore: false, count: 0},
  },
  action: ProfileAction,
): ProfileState => {
  switch (action.type) {
    case ProfileActionType.SET_USER_LIKED:
      return {
        ...prevState,
        liked: {
          list: action.liked.list,
          hasMore: action.liked.hasMore,
          count: action.liked.count,
        },
      };

    case ProfileActionType.SET_USER_GOING:
      return {
        ...prevState,
        going: {
          list: action.going.list,
          hasMore: action.going.hasMore,
          count: action.going.count,
        },
      };

    case ProfileActionType.SET_USER_PAST:
      return {
        ...prevState,
        past: {
          list: action.past.list,
          hasMore: action.past.hasMore,
          count: action.past.count,
        },
      };

    case ProfileActionType.ADD_USER_LIKED:
      return {
        ...prevState,
        liked: {
          ...prevState.liked,
          list: [...prevState.liked.list!, ...action.liked.list],
          hasMore: action.liked.hasMore,
        },
      };
    case ProfileActionType.ADD_USER_GOING:
      return {
        ...prevState,
        going: {
          ...prevState.going,
          list: [...prevState.going.list!, ...action.going.list],
          hasMore: action.going.hasMore,
        },
      };

    case ProfileActionType.ADD_USER_PAST:
      return {
        ...prevState,
        past: {
          ...prevState.past,
          list: [...prevState.past.list!, ...action.past.list],
          hasMore: action.past.hasMore,
        },
      };

    default:
      return {...prevState};
  }
};

export default profileReducer;
