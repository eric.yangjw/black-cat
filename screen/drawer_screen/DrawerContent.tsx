import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import DateSelectionSection from './components/DateSelectionSection';
import ChannelSelectionSection from './components/ChannelSelectionSection';
import {FromDateLogo, ToDateLogo} from '../../common/SVG';
import SearchButton from './components/SearchButton';
import DateInputField from './components/DateInputField';
import {useDispatch, useSelector} from 'react-redux';
import {
  DrawerContentComponentProps,
  DrawerContentOptions,
} from '@react-navigation/drawer';
import {DateType} from '../../common/enum/searchFilterType';
import {RootState} from '../../redux/store';
import {retrieveFilteredEventsAction} from '../feed_screen/redux/feedActions';

const DrawerContent = (
  props: DrawerContentComponentProps<DrawerContentOptions>,
) => {
  const moment = require('moment');

  const dispatch = useDispatch();
  const channels = useSelector((state: RootState) => state.search.channels);
  const [dateSelected, setDateSelected] = useState<DateType>(DateType.NONE);
  const [channelSelected, setChannelSelected] = useState<string>('');
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const updateDateSelection = (selected: DateType) => {
    setDateSelected(dateSelected === selected ? DateType.NONE : selected);
  };

  const updateChannelSelection = (selected: string) => {
    setChannelSelected(channelSelected === selected ? '' : selected);
  };

  const updateStartDate = (event: any, date?: Date) => {
    date != null && setStartDate(date);
  };

  const updateEndDate = (event: any, date?: Date) => {
    date != null && setEndDate(date);
  };

  const getSearchCriteria = () => {
    if (dateSelected === DateType.ANYTIME && channelSelected === 'All') {
      return 'All activities';
    }

    let date;
    if (dateSelected !== DateType.NONE) {
      if (dateSelected === DateType.ANYTIME) {
        date = 'anytime';
      } else if (dateSelected === DateType.LATER) {
        const start = moment(new Date(startDate)).format('DD/MM/YY');
        const end = moment(new Date(endDate)).format('DD/MM/YY');
        date = `${start} to ${end}`;
      } else {
        date = dateSelected;
      }

      if (channelSelected !== '') {
        return `${channelSelected} activities from ${date}`;
      } else {
        return `Activities from ${date}`;
      }
    } else {
      if (channelSelected !== '') {
        return `${channelSelected} activities`;
      }
    }

    return '';
  };

  const handleSearchOnPress = () => {
    if (dateSelected !== DateType.NONE || channelSelected !== '') {
      const criteria = getSearchCriteria();
      const selectedChannel = channels!.filter(
        channel =>
          channel.name === channelSelected && channelSelected !== 'All',
      );
      const channelId =
        selectedChannel.length > 0 ? selectedChannel[0].id : undefined;
      console.log('dispatch fileter criteria => ', criteria);
      dispatch(
        retrieveFilteredEventsAction(
          {
            offset: 0,
            channel: channelId,
            before: getBeforeDate(),
            after: getAfterDate(),
          },
          criteria,
        ),
      );
    }

    props.navigation.closeDrawer();
    props.navigation.navigate('Feed');
  };

  const getBeforeDate = (): number | undefined => {
    switch (dateSelected) {
      case DateType.ANYTIME || DateType.NONE:
        return undefined;
      case DateType.TODAY:
        return new Date(moment().format('DD MMM YYYY')).getTime();
      case DateType.TOMORROW:
        return new Date(
          moment().add(1, 'days').format('DD MMM YYYY'),
        ).getTime();
      case DateType.THIS_WEEK:
        return new Date(
          moment().startOf('week').format('DD MMM YYYY'),
        ).getTime();
      case DateType.THIS_MONTH:
        return new Date(
          moment().startOf('month').format('DD MMM YYYY'),
        ).getTime();
      case DateType.LATER:
        return startDate.getTime();
    }
  };

  const getAfterDate = (): number | undefined => {
    switch (dateSelected) {
      case DateType.ANYTIME || DateType.NONE:
        return undefined;
      case DateType.TODAY:
        return (
          new Date(moment().add(1, 'days').format('DD MMM YYYY')).getTime() - 1
        );
      case DateType.TOMORROW:
        return (
          new Date(moment().add(2, 'days').format('DD MMM YYYY')).getTime() - 1
        );
      case DateType.THIS_WEEK:
        return (
          new Date(
            moment().endOf('week').add(1, 'days').format('DD MMM YYYY'),
          ).getTime() - 1
        );

      case DateType.THIS_MONTH:
        return (
          new Date(
            moment().endOf('month').add(1, 'days').format('DD MMM YYYY'),
          ).getTime() - 1
        );
      case DateType.LATER:
        return endDate.getTime();
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.drawerSection}>
        <DateSelectionSection
          dateSelected={dateSelected}
          updateSelection={updateDateSelection}
        />
        {dateSelected === DateType.LATER ? (
          <View style={styles.dateRangeSection}>
            <DateInputField
              logo={<FromDateLogo />}
              date={startDate}
              updateDate={updateStartDate}
            />
            <DateInputField
              logo={<ToDateLogo />}
              date={endDate}
              updateDate={updateEndDate}
            />
          </View>
        ) : (
          <View style={styles.spacing} />
        )}

        <ChannelSelectionSection
          channelSelected={channelSelected.toString()}
          updateSelection={updateChannelSelection}
        />
      </View>
      <View style={styles.drawerSection}>
        <SearchButton
          handleSearchOnPress={handleSearchOnPress}
          getSearchCriteria={getSearchCriteria}
        />
      </View>
    </View>
  );
};

export default DrawerContent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 50,
    backgroundColor: '#453257',
    justifyContent: 'space-between',
    width: '100%',
  },
  drawerSection: {
    width: '100%',
  },
  spacing: {
    height: 60,
    marginVertical: 10,
  },
  dateRangeSection: {
    width: 250,
    height: 60,
    backgroundColor: 'white',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    marginVertical: 10,
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
});
