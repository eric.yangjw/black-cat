import React, {FC} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {SearchLogo} from '../../../common/SVG';

interface SearchButtonProps {
  getSearchCriteria: () => string;
  handleSearchOnPress: () => void;
}

const SearchButton: FC<SearchButtonProps> = ({
  getSearchCriteria,
  handleSearchOnPress,
}: SearchButtonProps) => {
  return (
    <TouchableOpacity onPress={handleSearchOnPress}>
      <View style={styles.searchSection}>
        <View style={styles.searchTextRow}>
          <SearchLogo height={28} width={28} color={'#453257'} />
          <Text style={styles.searchText}>Search</Text>
        </View>
        <View style={{width: 230, height: 55, flex: 1, flexWrap: 'wrap'}}>
          <Text style={styles.captionText}>{getSearchCriteria()}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default SearchButton;

const styles = StyleSheet.create({
  searchSection: {
    backgroundColor: '#D5EF7F',
    paddingVertical: 25,
    alignItems: 'center',
    width: '100%',
    height: 140,
  },
  searchTextRow: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  searchText: {
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 25,
    color: '#453257',
    paddingHorizontal: 5,
  },
  captionText: {
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 13,
    color: '#8560A9',
    paddingVertical: 10,
    width: '100%',
    textAlign: 'center',
  },
});
