import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

// Enum
import {DateType} from '../../../common/enum/searchFilterType';

type DataChipProps =
  | {
      type: 'date';
      name: string;
      dateSelected: DateType;
      chipDateType: DateType;
      updateDateSelection: (type: DateType) => void;
    }
  | {
      type: 'channel';
      key: number;
      name: string;
      channelSelected: string;
      updateChannelSelection: (name: string) => void;
    };

const isDateSelection = (props: DataChipProps): boolean =>
  props.type === 'date';

const isDateChipSelected = (props: DataChipProps): boolean =>
  props.type === 'date' && props.dateSelected === props.chipDateType;

const isChannelChipSelected = (props: DataChipProps): boolean =>
  props.type === 'channel' && props.channelSelected === props.name;

const updateState = (props: DataChipProps) => {
  if (props.type === 'date') {
    props.updateDateSelection(props.chipDateType);
  } else {
    props.updateChannelSelection(props.name);
  }
};

const DataChip = (props: DataChipProps) => (
  <TouchableOpacity
    onPress={() => updateState(props)}
    style={
      isDateChipSelected(props) || isChannelChipSelected(props)
        ? {
            ...styles.chipSelected,
            width: isDateSelection(props) || props.name === 'ALL' ? 80 : 'auto',
          }
        : {
            ...styles.chipNotSelected,
            width: isDateSelection(props) || props.name === 'ALL' ? 80 : 'auto',
          }
    }>
    <Text
      style={
        isDateChipSelected(props) || isChannelChipSelected(props)
          ? styles.chipTextSelected
          : styles.chipTextNotSelected
      }>
      {props.name}
    </Text>
  </TouchableOpacity>
);

export default DataChip;

const styles = StyleSheet.create({
  chipSelected: {
    backgroundColor: '#E5F7A9',
    borderRadius: 20,
    alignItems: 'center',
    padding: 5,
    width: 80,
    marginHorizontal: 3,
    marginVertical: 7,
  },
  chipNotSelected: {
    borderRadius: 20,
    alignItems: 'center',
    padding: 5,
    width: 80,
    marginHorizontal: 3,
    marginVertical: 7,
  },
  chipTextSelected: {
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 12,
    color: '#453257',
  },
  chipTextNotSelected: {
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 12,
    color: '#FFFFFF',
  },
});
