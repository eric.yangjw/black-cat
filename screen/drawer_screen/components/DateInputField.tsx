import React, {ReactNode} from 'react';
import {StyleSheet, View} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

interface DateInputFieldProps {
  logo: ReactNode;
  date: Date;
  updateDate: (event: any, date?: Date) => void;
}

const DateInputField = ({logo, date, updateDate}: DateInputFieldProps) => {
  return (
    <View style={styles.container}>
      {logo}
      <View style={styles.dateInputBox}>
        <DateTimePicker value={date} display="compact" onChange={updateDate} />
      </View>
    </View>
  );
};

export default DateInputField;

const styles = StyleSheet.create({
  container: {flexDirection: 'row', alignItems: 'center', margin: 5},
  dateInputBox: {width: 80, height: 30},
  dateText: {
    fontSize: 13,
    color: '#8560A9',
    paddingVertical: 10,
    width: '100%',
    textAlign: 'center',
  },
});
