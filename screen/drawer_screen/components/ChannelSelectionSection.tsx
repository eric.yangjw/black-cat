import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';

// Custom Components
import DataChip from './DataChip';

// Redux
import {RootState} from '../../../redux/store';
import {useDispatch, useSelector} from 'react-redux';
import {retrieveChannelList} from '../redux/searchActions';

interface ChannelSelectionSectionProps {
  channelSelected: string;
  updateSelection: (name: string) => void;
}

const ChannelSelectionSection = ({
  channelSelected,
  updateSelection,
}: ChannelSelectionSectionProps) => {
  const channels = useSelector((state: RootState) => state.search.channels!);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(retrieveChannelList());
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.channelContainer}>
        <Text style={styles.channelText}>Channel</Text>
      </View>
      <View style={styles.channelSection}>
        <View style={styles.row}>
          {channels &&
            channels.map((item, idx) => (
              <DataChip
                type="channel"
                key={idx}
                name={item.name}
                channelSelected={channelSelected}
                updateChannelSelection={updateSelection}
              />
            ))}
        </View>
      </View>
    </View>
  );
};

export default ChannelSelectionSection;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  channelContainer: {
    borderBottomWidth: 1,
    borderColor: '#AC8EC9',
    padding: 5,
  },
  channelText: {
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 17,
    color: '#AC8EC9',
  },
  channelSection: {
    paddingVertical: 5,
    width: 250,
  },
  row: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});
