import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

// Custom Components
import DataChip from './DataChip';

// Enum
import {DateType} from '../../../common/enum/searchFilterType';

interface DateSelectionSectionProps {
  dateSelected: DateType;
  updateSelection: (type: DateType) => void;
}

const DateSelectionSection = ({
  dateSelected,
  updateSelection,
}: DateSelectionSectionProps) => (
  <View style={styles.container}>
    <View style={styles.dateContainer}>
      <Text style={styles.dateText}>Date</Text>
    </View>
    <View style={styles.dateSection}>
      <View style={styles.row}>
        <DataChip
          type="date"
          chipDateType={DateType.ANYTIME}
          name={'ANYTIME'}
          dateSelected={dateSelected}
          updateDateSelection={updateSelection}
        />
        <DataChip
          type="date"
          chipDateType={DateType.TODAY}
          name={'TODAY'}
          dateSelected={dateSelected}
          updateDateSelection={updateSelection}
        />
        <DataChip
          type="date"
          chipDateType={DateType.TOMORROW}
          name={'TOMORROW'}
          dateSelected={dateSelected}
          updateDateSelection={updateSelection}
        />
      </View>
      <View style={styles.row}>
        <DataChip
          type="date"
          chipDateType={DateType.THIS_WEEK}
          name={'THIS WEEK'}
          dateSelected={dateSelected}
          updateDateSelection={updateSelection}
        />
        <DataChip
          type="date"
          chipDateType={DateType.THIS_MONTH}
          name={'THIS MONTH'}
          dateSelected={dateSelected}
          updateDateSelection={updateSelection}
        />
      </View>
      <View style={styles.row}>
        <DataChip
          type="date"
          chipDateType={DateType.LATER}
          name={'LATER'}
          dateSelected={dateSelected}
          updateDateSelection={updateSelection}
        />
      </View>
    </View>
  </View>
);

export default DateSelectionSection;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  dateContainer: {
    borderBottomWidth: 1,
    borderColor: '#AC8EC9',
    padding: 5,
    width: 44,
  },
  dateText: {
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 17,
    color: '#AC8EC9',
  },
  dateSection: {paddingVertical: 5},
  row: {flexDirection: 'row'},
});
