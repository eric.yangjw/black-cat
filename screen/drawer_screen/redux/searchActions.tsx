import {AxiosResponse} from 'axios';
import {Channel, ChannelJson} from '../../../model/channel';
import {fetchChannelListFromSever} from '../../../service/searchService';

export enum SearchActionType {
  SET_CHANNELS = 'SET_CHANNELS',
}

export interface SearchAction {
  type: SearchActionType.SET_CHANNELS;
  channels: Channel[];
}

export const retrieveChannelList =
  () => (dispatch: (action: SearchAction) => void, getState: any) => {
    fetchChannelListFromSever()
      .then((res: AxiosResponse) => {
        dispatch(_setChannelsAction(res.data.channels));
      })
      .catch((e: TypeError) =>
        console.log(`### retrieveChannelList statusCode: ${e.message}`),
      );
  };

const _setChannelsAction = (channels: ChannelJson[]): SearchAction => {
  return {
    type: SearchActionType.SET_CHANNELS,
    channels: [Channel.getChannelNamedAll(), ...Channel.listFromData(channels)],
  };
};
