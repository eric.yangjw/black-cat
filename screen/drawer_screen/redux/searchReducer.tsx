import {Channel} from '../../../model/channel';
import {SearchAction, SearchActionType} from './searchActions';

interface SearchState {
  channels: Channel[] | null;
}

const searchReducer = (
  prevState: SearchState = {channels: null},
  action: SearchAction,
): SearchState => {
  switch (action.type) {
    case SearchActionType.SET_CHANNELS:
      return {...prevState, channels: action.channels};

    default:
      return {...prevState};
  }
};

export default searchReducer;
