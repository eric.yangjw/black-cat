import React, {FC} from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';
import {
  useFonts,
  SourceSansPro_600SemiBold,
} from '@expo-google-fonts/source-sans-pro';
import AppLoading from 'expo-app-loading';

interface ButtonProps {
  handleSignIn: (navigation: any) => void;
}

const SignInButtonSection: FC<ButtonProps> = (props, {navigation}) => {
  const [fontsLoaded] = useFonts({SourceSansPro_600SemiBold});

  if (!fontsLoaded) return <AppLoading />;

  return (
    <TouchableOpacity
      style={styles.button}
      onPress={() => props.handleSignIn(navigation)}>
      <Text style={styles.signInText}>Sign In</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    height: '30%',
    width: '100%',
    backgroundColor: '#D5EF7F',
    justifyContent: 'center',
    alignItems: 'center',
  },
  signInText: {
    color: '#453257',
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 16,
  },
});

export default SignInButtonSection;
