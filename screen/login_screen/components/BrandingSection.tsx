import React from 'react';
import {StyleSheet, Text, View, ViewStyle, StyleProp} from 'react-native';

// Custom Components
import {BlackCatLogo} from '../../../common/SVG';

interface BrandingSectionProps {
  style: StyleProp<ViewStyle>;
}

const BrandingSection = ({style}: BrandingSectionProps) => {
  return (
    <View style={style}>
      <View style={styles.container}>
        <Text style={styles.header}>FIND THE MOST LOVED ACTIVITIES</Text>
        <Text style={styles.title}>BLACK CAT</Text>
        <View style={styles.logo}>
          <BlackCatLogo />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    height: 50,
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 20,
    color: '#D5EF7F',
  },
  title: {
    fontFamily: 'SourceSansPro_700Bold',
    fontSize: 30,
    color: '#D5EF7F',
  },
  logo: {
    height: 84,
    width: 84,
    borderWidth: 2,
    borderRadius: 42,
    borderColor: '#D5EF7F',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 40,
  },
});

export default BrandingSection;
