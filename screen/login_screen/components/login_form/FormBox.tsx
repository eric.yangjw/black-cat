import React, {ReactNode, useState} from 'react';
import {StyleProp, StyleSheet, TextInput, View, ViewStyle} from 'react-native';

interface FormBoxProps {
  style: StyleProp<ViewStyle>;
  icon: ReactNode;
  field: string;
  onChange: (key: string, value: string) => void;
}

interface BackgroundColorState {
  onFocus: boolean;
  color: string;
}

const FormBox = ({style, icon, field, onChange}: FormBoxProps) => {
  const [backgroundColor, setBGColor] = useState<BackgroundColorState>({
    onFocus: false,
    color: 'rgba(255, 255, 255, 0)',
  });

  const handleFormBoxTap = () => {
    if (backgroundColor.onFocus) {
      setBGColor({
        onFocus: false,
        color: 'rgba(255, 255, 255, 0)',
      });
    } else {
      setBGColor({
        onFocus: true,
        color: 'rgba(255, 255, 255, 0.5)',
      });
    }
  };

  return (
    <View style={{...styles.container, backgroundColor: backgroundColor.color}}>
      {icon}
      <TextInput
        style={styles.text}
        autoCapitalize="none"
        placeholder={field}
        secureTextEntry={field === 'Password'}
        placeholderTextColor="#E8E8E8"
        onFocus={() => handleFormBoxTap()}
        onChange={event => onChange(field, event.nativeEvent.text)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderWidth: 2,
    width: 240,
    height: 40,
    borderRadius: 20,
    borderColor: '#E8E8E8',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 5,
    marginVertical: 16,
  },
  text: {
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 12,
    color: '#453257',
  },
});

export default FormBox;
