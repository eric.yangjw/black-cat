import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Alert, StyleProp, StyleSheet, View, ViewStyle} from 'react-native';

// Custom Components
import SignInButtonSection from './SignInButtonSection';
import FormBox from './login_form/FormBox';
import {UserLogo, PasswordLogo} from '../../../common/SVG';

// Redux
import {RootState} from '../../../redux/store';
import {authenticateUserAction, clearErrMsgAction} from '../redux/authActions';

export interface FormState {
  username: string;
  password: string;
}

interface FormSectionProps {
  style: StyleProp<ViewStyle>;
}

const FormSection = ({style}: FormSectionProps) => {
  const dispatch = useDispatch();
  const errorMessage = useSelector(
    (state: RootState) => state.auth.errorMessage,
  );

  const [formState, setFormState] = useState<FormState>({
    username: '',
    password: '',
  });

  const updateFormState = (name: string, value: string) => {
    setFormState((prevState: FormState) => {
      if (name === 'Username') {
        return {...prevState, username: value};
      } else if (name === 'Password') {
        return {...prevState, password: value};
      } else {
        return {...prevState};
      }
    });
  };

  const handleSignIn = async () => dispatch(authenticateUserAction(formState));

  const clearLogInErrorMessage = () => dispatch(clearErrMsgAction());

  if (errorMessage) {
    Alert.alert('Log In Error', errorMessage, [
      {
        text: 'Ok',
        onPress: () => clearLogInErrorMessage(),
      },
    ]);
  }

  return (
    <View style={style}>
      <View>
        <FormBox
          style={styles.container}
          icon={<UserLogo />}
          field="Username"
          onChange={updateFormState}
        />
        <FormBox
          style={styles.container}
          icon={<PasswordLogo />}
          field="Password"
          onChange={updateFormState}
        />
      </View>
      <SignInButtonSection handleSignIn={async () => await handleSignIn()} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default FormSection;
