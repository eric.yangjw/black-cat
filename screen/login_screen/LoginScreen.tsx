import React from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  KeyboardAvoidingView,
} from 'react-native';

// Custom Components
import BrandingSection from './components/BrandingSection';
import FormSection from './components/FormSection';
import DismissKeyboard from '../../components/DismissKeyboard';

const LoginScreen = () => {
  const backgroundImage = require('../../assets/street-dance.jpg');

  return (
    <DismissKeyboard>
      <KeyboardAvoidingView behavior="padding">
        <ImageBackground source={backgroundImage} style={styles.container}>
          <View style={styles.overlay}>
            <BrandingSection style={styles.branding} />
            <FormSection style={styles.form}></FormSection>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    </DismissKeyboard>
  );
};

const styles = StyleSheet.create({
  container: {},
  overlay: {
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(133, 96, 169, 0.7)',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  branding: {flex: 5, justifyContent: 'center'},
  form: {
    flex: 3,
    alignItems: 'center',
    height: '100%',
    width: '100%',
    justifyContent: 'space-between',
  },
});

export default LoginScreen;
