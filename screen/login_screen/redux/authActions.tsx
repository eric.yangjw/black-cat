import {AxiosError, AxiosResponse} from 'axios';
import {FormState} from '../components/FormSection';
import {AuthUser, AuthUserJson} from '../../../model/authUser';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  authenticateWithServer,
  retrieveUserFromServer,
} from '../../../service/authService';

interface AuthUserRes {
  token: string;
  user: AuthUserJson;
}

export enum AuthActionType {
  RETRIEVE_TOKEN = 'RETRIEVE_TOKEN',
  LOG_IN = 'LOG_IN',
  SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE',
  CLEAR_ERROR_MESSAGE = 'CLEAR_ERROR_MESSAGE',
  LOG_OUT = 'CLEAR_ERROR_MESSAGE',
}

export type AuthAction =
  | {
      type: AuthActionType.LOG_IN;
      token: string;
      authUser: AuthUser;
    }
  | {
      type: AuthActionType.LOG_OUT;
    }
  | {
      type: AuthActionType.SET_ERROR_MESSAGE;
      errorMessage: string;
    }
  | {
      type: AuthActionType.CLEAR_ERROR_MESSAGE;
    }
  | {
      type: AuthActionType.RETRIEVE_TOKEN;
    };

/* ===== Actions ===== */
export const authenticateUserAction =
  (credentials: FormState) =>
  (dispatch: (action: AuthAction) => void, getState: any): void => {
    authenticateWithServer(credentials)
      .then((res: AxiosResponse) => {
        const {token, user}: AuthUserRes = res.data;
        dispatch(_logInAction(token, user));
        return token;
      })
      .then(async token => await _storeToken(token))
      .catch((e: AxiosError) => {
        if (e.response!.status === 403) {
          dispatch(_setErrMsgAction('Invalid Username/Password'));
        } else {
          console.log(`### authenticateUser statusCode: ${e.response!.status}`);
        }
      });
  };

export const retrieveUserAction =
  () =>
  (dispatch: (action: AuthAction) => void, getState: any): void => {
    _retrieveToken()
      .then((token: string | null) => {
        if (token !== null) {
          retrieveUserFromServer(token)
            .then((res: AxiosResponse) => {
              const {token, user}: AuthUserRes = res.data;
              dispatch(_logInAction(token, user));
            })
            .catch((e: TypeError) => {
              console.log(`### retrieveUser statusCode: ${e.message}`);
            });
        }
      })
      .catch((e: TypeError) => console.log(e.message));
  };

const _logInAction = (token: string, user: AuthUserJson): AuthAction => {
  return {
    type: AuthActionType.LOG_IN,
    token,
    authUser: AuthUser.fromData(user),
  };
};

const _setErrMsgAction = (errorMessage: string): AuthAction => {
  return {
    type: AuthActionType.SET_ERROR_MESSAGE,
    errorMessage: errorMessage,
  };
};

export const clearErrMsgAction = (): AuthAction => {
  return {
    type: AuthActionType.CLEAR_ERROR_MESSAGE,
  };
};

export const logOutUserAction =
  () =>
  (dispatch: (action: AuthAction) => void, getState: any): void => {
    _clearToken()
      .then(() => dispatch(_logOutAction()))
      .catch((e: TypeError) => {
        console.log(`### Error logOutUserAction: ${e.message}`);
      });
  };

const _logOutAction: () => AuthAction = () => {
  return {
    type: AuthActionType.LOG_OUT,
    token: null,
    authUser: null,
    errorMessage: '',
  };
};

const _storeToken = async (token: string): Promise<void> => {
  try {
    await AsyncStorage.setItem('token', token);
  } catch (e) {
    console.log('>>> Error is storing token');
  }
};

const _retrieveToken = async (): Promise<string> => {
  try {
    const value = await AsyncStorage.getItem('token');
    return value !== null ? value : '';
  } catch (e) {
    return '';
  }
};

const _clearToken = async () => {
  try {
    await AsyncStorage.removeItem('token');
  } catch (e) {
    console.log('>>> Error is removing token');
  }
};
