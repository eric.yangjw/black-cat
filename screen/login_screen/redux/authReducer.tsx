import {AuthUser} from '../../../model/authUser';
import {AuthAction, AuthActionType} from './authActions';

export interface AuthState {
  token: string | undefined;
  authUser: AuthUser | undefined;
  isLoggedIn: boolean;
  errorMessage: string;
}

const authReducer = (
  state: AuthState = {
    isLoggedIn: false,
    authUser: undefined,
    errorMessage: '',
    token: undefined,
  },
  action: AuthAction,
): AuthState => {
  switch (action.type) {
    case AuthActionType.LOG_IN:
      return {
        token: action.token,
        authUser: action.authUser,
        isLoggedIn: true,
        errorMessage: '',
      };

    case AuthActionType.LOG_OUT:
      return {
        token: undefined,
        authUser: undefined,
        isLoggedIn: false,
        errorMessage: '',
      };

    case AuthActionType.SET_ERROR_MESSAGE:
      return {...state, errorMessage: action.errorMessage};

    case AuthActionType.CLEAR_ERROR_MESSAGE:
      return {...state, errorMessage: ''};

    default:
      return {...state};
  }
};

export default authReducer;
