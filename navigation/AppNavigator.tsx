import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import AuthNavStack from './AuthNavStack';

import {useSelector} from 'react-redux';
import {RootState} from '../redux/store';
import AppNavStack from './AppNavStack';

const AppNavigator = () => {
  const {isLoggedIn} = useSelector((state: RootState) => state.auth);
  return (
    <NavigationContainer>
      {!isLoggedIn ? <AuthNavStack /> : <AppNavStack />}
    </NavigationContainer>
  );
};

export default AppNavigator;
