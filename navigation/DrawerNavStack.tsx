import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';

import DrawerContent from '../screen/drawer_screen/DrawerContent';
import FeedScreen from '../screen/feed_screen/FeedScreen';

const Drawer = createDrawerNavigator();

const DrawerNavStack = () => {
  return (
    <Drawer.Navigator
      screenOptions={{headerShown: false}}
      drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="Feed_Drawer" component={FeedScreen} />
    </Drawer.Navigator>
  );
};

export default DrawerNavStack;
