import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from '../screen/login_screen/LoginScreen';

const Auth = createStackNavigator();
const screenOptions = {headerShown: false};

const AuthNavStack = () => {
  return (
    <Auth.Navigator screenOptions={screenOptions}>
      <Auth.Screen name="Login" component={LoginScreen} />
    </Auth.Navigator>
  );
};

export default AuthNavStack;
