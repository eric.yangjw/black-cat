import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// import screens
import ProfileScreen from '../screen/profile_screen/ProfileScreen';
import EventDetailScreen from '../screen/event_details_screen/EventDetailScreen';
import DrawerNavStack from './DrawerNavStack';

const Stack = createStackNavigator();
const screenOptions = {headerShown: false};

const AppNavStack = () => {
  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen name="Feed" component={DrawerNavStack} />
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="Event_Details" component={EventDetailScreen} />
    </Stack.Navigator>
  );
};
export default AppNavStack;
