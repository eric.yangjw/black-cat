import {AxiosResponse} from 'axios';
import {API_URL} from '../common/constants';
import {ProfileContentType} from '../common/enum/contentType';
import {AppEventJson} from '../model/appEvent';

const axios = require('axios');

export const fetchAuthUserEventListFromServer = async (
  userId: number,
  type: ProfileContentType,
  offset: number,
): Promise<AxiosResponse> => {
  return axios.get(`${API_URL}/user/${userId}/events`, {
    params: {
      type,
      offset,
    },
  });
};

export const fetchTotalEventCountFromServer = async (
  userId: number,
  type: ProfileContentType,
  existingLength: number,
): Promise<number> => {
  let count = existingLength;
  let hasMore = true;
  while (hasMore) {
    const res = await axios.get(`${API_URL}/user/${userId}/events`, {
      params: {
        type,
        offset: count,
      },
    });
    const {events, has_more}: {events: AppEventJson[]; has_more: boolean} =
      res.data;

    count += events.length;
    hasMore = has_more;
  }

  return count;
};
