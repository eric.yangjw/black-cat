import {AxiosResponse} from 'axios';
import {API_URL} from '../common/constants';
import {FormState} from '../screen/login_screen/components/FormSection';

const axios = require('axios');

export const authenticateWithServer = async (
  credentials: FormState,
): Promise<AxiosResponse> => {
  return axios.post(`${API_URL}/auth/token`, credentials);
};

export const retrieveUserFromServer = async (
  token: string,
): Promise<AxiosResponse> => {
  return axios.get(`${API_URL}/auth/user`, {params: {token}});
};
