import {AxiosResponse} from 'axios';
import {API_URL} from '../common/constants';

const axios = require('axios');

/* ===== Likes ===== */
export const addLikeToEventInServer = async (
  eventId: number,
): Promise<AxiosResponse> => {
  return axios.post(`${API_URL}/event/${eventId}/likes`);
};

export const removeLikeFromEventInServer = async (
  eventId: number,
  userId: number,
): Promise<AxiosResponse> => {
  return axios.delete(`${API_URL}/event/${eventId}/likes/${userId}`);
};

/* ===== Going ===== */
export const addGoingToEventInServer = async (
  eventId: number,
): Promise<AxiosResponse> => {
  return axios.post(`${API_URL}/event/${eventId}/participants`);
};

export const removeGoingFromEventInServer = async (
  eventId: number,
  userId: number,
): Promise<AxiosResponse> => {
  return axios.delete(`${API_URL}/event/${eventId}/participants/${userId}`);
};

/* ===== Comment ===== */
export const addCommentToEventInServer = async (
  eventId: number,
  comment: string,
): Promise<AxiosResponse> => {
  return axios.post(`${API_URL}/event/${eventId}/comments`, {comment});
};
