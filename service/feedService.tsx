import {AxiosResponse} from 'axios';
import {API_URL} from '../common/constants';

const axios = require('axios');

export interface FilteredEventParams {
  offset?: number;
  channel?: number;
  before?: number;
  after?: number;
}

export const fetchFeedFromServer = async (): Promise<AxiosResponse> => {
  return axios
    .get(`${API_URL}/events`)
    .catch((e: TypeError) =>
      console.log('=== Error in getFeedFromServer: ', e),
    );
};

export const fetchFilteredEventsFromSever = async ({
  offset,
  channel,
  before,
  after,
}: FilteredEventParams): Promise<AxiosResponse> => {
  return axios.get(`${API_URL}/events`, {
    params: {
      offset,
      channel,
      before,
      after,
    },
  });
};
