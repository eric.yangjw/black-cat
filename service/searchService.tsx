import {AxiosResponse} from 'axios';
import {API_URL} from '../common/constants';

const axios = require('axios');

export const fetchChannelListFromSever = async (): Promise<AxiosResponse> => {
  return axios.get(`${API_URL}/channels`);
};
