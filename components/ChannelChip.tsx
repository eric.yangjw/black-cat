import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

// Props
import {EventCardProps} from './event_card/EventCard';

const ChannelChip = ({event}: EventCardProps) => {
  return (
    <View style={styles.channelChip}>
      <Text style={styles.channelName}>{event.channel.name}</Text>
    </View>
  );
};

export default ChannelChip;

const styles = StyleSheet.create({
  channelChip: {
    alignSelf: 'flex-start',
    borderWidth: 1,
    borderColor: '#D3C1E5',
    borderRadius: 50,
    paddingHorizontal: 10,
    justifyContent: 'center',
    marginHorizontal: 5,
  },
  channelName: {
    color: '#8560A9',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 15,
  },
});
