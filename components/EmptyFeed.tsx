import React from 'react';
import {StyleProp, StyleSheet, Text, View, ViewStyle} from 'react-native';

// Custom Components
import {NoActivityLogo} from '../common/SVG';

interface EmptyFeedProps {
  style?: StyleProp<ViewStyle>;
}

const EmptyFeed = ({style}: EmptyFeedProps) => {
  return (
    <View style={[styles.container, style]}>
      <NoActivityLogo height={50} width={50} />
      <Text style={styles.text}>No Activity Found</Text>
    </View>
  );
};

export default EmptyFeed;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingVertical: 100,
    // justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    minHeight: 720,
  },
  text: {
    padding: 15,
    color: '#BABABA',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 23,
  },
});
