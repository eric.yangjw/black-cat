import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

// Custom Components
import {
  CheckLogo,
  CheckOutlineLogo,
  LikeLogo,
  LikeOutlineLogo,
} from '../../../common/SVG';

// Redux
import {RootState} from '../../../redux/store';
import {useSelector} from 'react-redux';

// Props
import {EventCardProps} from '../EventCard';

const EventCardActionSection = ({event}: EventCardProps) => {
  const {goingsCount, likesCount, goings, likes} = event;
  const userId = useSelector((state: RootState) => state.auth.authUser!.id);
  const isGoing = goings.find(going => going.id === userId) !== undefined;
  const hasLiked = likes.find(like => like.id === userId) !== undefined;

  const getLikeCountMessage = (count: number): string => {
    if (count <= 1) return `${count} Like`;
    return `${count} Likes`;
  };

  return (
    <View style={styles.container}>
      <View style={styles.goingAction}>
        {isGoing ? <CheckLogo /> : <CheckOutlineLogo />}
        <Text>{isGoing ? 'I am going' : `${goingsCount} Going`}</Text>
      </View>
      <View style={styles.likeAction}>
        {hasLiked ? <LikeLogo /> : <LikeOutlineLogo />}
        <Text>{hasLiked ? 'I like it' : getLikeCountMessage(likesCount)}</Text>
      </View>
    </View>
  );
};

export default EventCardActionSection;

const styles = StyleSheet.create({
  container: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 5,
    marginBottom: 10,
  },
  likeAction: {
    flexDirection: 'row',
  },
  goingAction: {
    flexDirection: 'row',
  },
  username: {
    paddingHorizontal: 10,
    color: '#67616D',
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 15,
  },
  channelChip: {
    borderWidth: 1,
    borderColor: '#D3C1E5',
    borderRadius: 50,
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  channelName: {
    color: '#8560A9',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 15,
  },
});
