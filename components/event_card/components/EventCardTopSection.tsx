import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

// Custom Components
import {Avatar} from 'react-native-elements/dist/avatar/Avatar';
import ChannelChip from '../../ChannelChip';

// Props
import {EventCardProps} from '../EventCard';

const EventCardTopSection = ({event}: EventCardProps) => {
  const {avatar, username} = event.creator;
  return (
    <View style={styles.topSection}>
      <View style={styles.user}>
        <Avatar rounded size="small" source={{uri: avatar}} />
        <Text style={styles.username}>{username}</Text>
      </View>
      <ChannelChip event={event} />
    </View>
  );
};

export default EventCardTopSection;

const styles = StyleSheet.create({
  topSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 5,
    marginBottom: 10,
  },
  user: {
    flexDirection: 'row',

    width: '50%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  username: {
    paddingHorizontal: 10,
    color: '#67616D',
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 15,
  },
});
