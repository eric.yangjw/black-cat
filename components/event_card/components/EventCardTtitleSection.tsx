import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

// Custom Components
import {TimeLogo} from '../../../common/SVG';

// Props
import {EventCardProps} from './../EventCard';
const moment = require('moment');

const EventCardTitleSection = ({event}: EventCardProps) => {
  const getDuration = (beginTime: number, endTime: number) => {
    const start = moment(new Date(beginTime)).format('DD MMM YYYY hh:mm');
    const end = moment(new Date(endTime)).format('DD MMM YYYY hh:mm');
    return `${start} - ${end}`;
  };

  return (
    <View style={styles.container}>
      <View
        style={[
          styles.titleSection,
          {width: event.images[0] ? '75%' : '100%'},
        ]}>
        <View style={styles.titleBox}>
          <Text style={styles.titleText}>{event.name}</Text>
        </View>
        <View style={styles.dateBox}>
          <TimeLogo />
          <Text style={styles.dateText}>
            {getDuration(event.beginTime, event.endTime)}
          </Text>
        </View>
      </View>

      {event.images.length !== 0 && (
        <View>
          <Image style={styles.image} source={{uri: event.images[0]}} />
        </View>
      )}
    </View>
  );
};

export default EventCardTitleSection;

const styles = StyleSheet.create({
  container: {
    // height: 110,
    flexDirection: 'row',
    marginHorizontal: 5,
    marginBottom: 10,
    justifyContent: 'space-between',
  },
  titleSection: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  titleBox: {
    flexDirection: 'row',
  },
  titleText: {
    height: '100%',
    width: '100%',
    color: '#453257',
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 23,
    flexWrap: 'wrap',
  },
  image: {
    width: 80,
    height: 80,
  },
  dateBox: {
    paddingTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  dateText: {
    color: '#8560A9',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 15,
  },
});
