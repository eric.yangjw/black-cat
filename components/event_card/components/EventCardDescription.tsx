import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

// Props
import {EventCardProps} from './../EventCard';

const EventCardDescriptionSection = ({event}: EventCardProps) => {
  return (
    <View style={[styles.container, {width: event.images[0] ? '73%' : '100%'}]}>
      <Text numberOfLines={3} style={styles.descriptionText}>
        {event.description}
      </Text>
    </View>
  );
};

export default EventCardDescriptionSection;

const styles = StyleSheet.create({
  container: {
    height: 60,
    flexDirection: 'row',
    marginHorizontal: 5,
    marginBottom: 10,
    justifyContent: 'space-between',
  },
  descriptionText: {
    color: '#67616D',
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 14,
  },
});
