import React from 'react';
import {StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

// Custom Components
import EventCardTopSection from './components/EventCardTopSection';
import EventCardTitleSection from './components/EventCardTtitleSection';
import EventCardDescriptionSection from './components/EventCardDescription';
import EventCardActionSection from './components/EventCardActionSection';

// Model
import {AppEvent} from '../../model/appEvent';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../redux/store';
import {setEventAction} from '../../screen/event_details_screen/redux/eventActions';

export interface EventCardProps {
  event: AppEvent;
}

const EventCard = ({event}: EventCardProps) => {
  const userId = useSelector((state: RootState) => state.auth.authUser!.id);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const navigateToEventDetails = (): void => {
    dispatch(setEventAction(event, userId));
    navigation.navigate('Event_Details');
  };

  return (
    <React.Fragment>
      <TouchableOpacity
        style={styles.container}
        onPress={() => navigateToEventDetails()}>
        <EventCardTopSection event={event} />
        <EventCardTitleSection event={event} />
        <EventCardDescriptionSection event={event} />
        <EventCardActionSection event={event} />
      </TouchableOpacity>
    </React.Fragment>
  );
};

export default EventCard;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    // height: 265,
  },
});
