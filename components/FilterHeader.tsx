import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

// Redux
import {RootState} from '../redux/store';
import {useDispatch, useSelector} from 'react-redux';
import {clearFilteredEventsAction} from '../screen/feed_screen/redux/feedActions';

const FilterHeader = ({clearFilter}: {clearFilter: () => void}) => {
  const dispatch = useDispatch();
  const {filteredList, criteria} = useSelector(
    (state: RootState) => state.feed,
  );

  const clearFilterHeader = () => {
    dispatch(clearFilteredEventsAction());
    clearFilter();
  };

  const getSearchCount = () => {
    if (filteredList!.length <= 1) return `${filteredList!.length} result`;
    return `${filteredList!.length} results`;
  };

  return (
    <View style={styles.container}>
      <View style={styles.topSection}>
        <View style={styles.resultsTextSection}>
          <Text style={styles.resultText}>{getSearchCount()}</Text>
        </View>
        <TouchableOpacity
          style={styles.clearButton}
          onPress={clearFilterHeader}>
          <Text style={styles.clearButtonText}>CLEAR SEARCH</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.criteriaSection}>
        <Text>{`Searched for ${criteria}`}</Text>
      </View>
    </View>
  );
};

export default FilterHeader;

const styles = StyleSheet.create({
  container: {
    height: 100,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: 'white',
  },
  topSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  resultsTextSection: {},
  resultText: {
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 20,
    color: '#8560A9',
  },
  clearButton: {
    backgroundColor: '#D5EF7F',
    height: 35,
    width: 100,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  clearButtonText: {
    fontFamily: 'SourceSansPro_600SemiBold',
    fontSize: 11,
    color: '#67616D',
  },
  criteriaSection: {
    paddingTop: 10,
  },
  criteriaText: {
    fontFamily: 'SourceSansPro_400Regular',
    fontSize: 12,
    color: '#67616D',
  },
});
