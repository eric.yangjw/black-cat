import React from 'react';
import {StyleSheet, View} from 'react-native';

import {BlackCatLogo} from '../../common/SVG';
import SearchButton from './components/SearchButton';
import ProfileAvatar from './components/ProfileAvatar';
import HomeButton from './components/HomeButtom';

const AppHeader = ({isFeed = false}: {isFeed?: boolean}) => {
  return (
    <View style={styles.container}>
      {!isFeed ? <HomeButton /> : <SearchButton />}
      <BlackCatLogo />
      <ProfileAvatar />
    </View>
  );
};

export default AppHeader;

const styles = StyleSheet.create({
  container: {
    height: 85,
    width: '100%',
    backgroundColor: '#8560A9',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingBottom: 10,
  },
});
