import React from 'react';
import {
  NavigationProp,
  ParamListBase,
  useNavigation,
} from '@react-navigation/native';
import {StyleSheet, View} from 'react-native';
import {Avatar} from 'react-native-elements';

import {useSelector} from 'react-redux';
import {RootState} from '../../../redux/store';

const ProfileAvatar = () => {
  const navigation: NavigationProp<ParamListBase> = useNavigation();
  const authUser = useSelector((state: RootState) => state.auth.authUser);

  return (
    <View style={styles.container}>
      <Avatar
        rounded
        size="small"
        source={{uri: authUser!.avatar}}
        onPress={() => navigation.navigate('Profile')}
      />
    </View>
  );
};

export default ProfileAvatar;

const styles = StyleSheet.create({
  container: {
    height: 50,
    width: 50,
    borderRadius: 13,
    marginHorizontal: 10,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
