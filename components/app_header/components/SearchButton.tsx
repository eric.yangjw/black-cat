import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {useNavigation, DrawerActions} from '@react-navigation/native';

import {SearchLogo} from '../../../common/SVG';

const SearchButton = () => {
  const navigation: any = useNavigation();

  return (
    <TouchableOpacity
      style={styles.button}
      onPress={() => {
        navigation.dispatch(DrawerActions.openDrawer());
      }}>
      <SearchLogo height={25} width={25} />
    </TouchableOpacity>
  );
};

export default SearchButton;

const styles = StyleSheet.create({
  button: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
