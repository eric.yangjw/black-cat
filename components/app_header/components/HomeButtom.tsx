import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {HomeLogo} from '../../../common/SVG';

const HomeButton = () => {
  const navigation: any = useNavigation();

  return (
    <TouchableOpacity
      style={styles.button}
      onPress={() => {
        navigation.navigate('Feed');
      }}>
      <HomeLogo height={27} width={27} />
    </TouchableOpacity>
  );
};

export default HomeButton;

const styles = StyleSheet.create({
  button: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
