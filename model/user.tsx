export interface UserJson {
  id: number;
  username: string;
  avatar: string;
}

export class User {
  id: number;
  username: string;
  avatar: string;

  constructor(data: UserJson) {
    this.id = data.id;
    this.username = data.username;
    this.avatar = data.avatar;
  }

  toJson(): UserJson {
    return {
      id: this.id,
      username: this.username,
      avatar: this.avatar,
    };
  }

  static fromData(data: UserJson): User {
    return new User(data);
  }

  static listFromData(jsonList: UserJson[]): User[] {
    return jsonList.map(data => User.fromData(data));
  }

  static toJsonList(userList: User[]): UserJson[] {
    return userList.map(user => user.toJson());
  }
}
