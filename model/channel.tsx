export interface ChannelJson {
  id: number;
  name: string;
}

export class Channel {
  id: number;
  name: string;

  constructor(data: ChannelJson) {
    this.id = data.id;
    this.name = data.name;
  }

  static getChannelNamedAll(): Channel {
    return new Channel({
      id: -1,
      name: 'All',
    });
  }

  toJson(): ChannelJson {
    return {
      id: this.id,
      name: this.name,
    };
  }

  static fromData(data: ChannelJson): Channel {
    return new Channel(data);
  }

  static listFromData(dataList: ChannelJson[]): Channel[] {
    return dataList.map(data => new Channel(data));
  }

  static toJsonList(channelList: Channel[]): ChannelJson[] {
    return channelList.map(channel => channel.toJson());
  }
}
