export interface AuthUserJson {
  id: number;
  username: string;
  avatar: string;
  email: string;
  likes_count: number;
  goings_count: number;
  past_count: number;
}

export class AuthUser {
  id: number;
  username: string;
  avatar: string;
  email: string;
  likesCount: number;
  goingsCount: number;
  pastCount: number;

  constructor(data: AuthUserJson) {
    this.id = data.id;
    this.username = data.username;
    this.avatar = data.avatar;
    this.email = data.email;
    this.likesCount = data.likes_count;
    this.goingsCount = data.goings_count;
    this.pastCount = data.past_count;
  }

  toJson(): AuthUserJson {
    return {
      id: this.id,
      username: this.username,
      avatar: this.avatar,
      email: this.email,
      likes_count: this.likesCount,
      goings_count: this.goingsCount,
      past_count: this.pastCount,
    };
  }

  static fromData(data: AuthUserJson): AuthUser {
    return new AuthUser(data);
  }
}
