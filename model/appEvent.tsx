import {Channel, ChannelJson} from './channel';
import {User, UserJson} from './user';
import {Comment, CommentJson} from './comment';

export interface AppEventJson {
  id: number;
  name: string;
  begin_time: number;
  end_time: number;
  description: string;
  creator: UserJson;
  create_time: number;
  update_time: number;
  channel: ChannelJson;
  goings: UserJson[];
  likes: UserJson[];
  images: string[];
  location: string;
  goings_count: number;
  likes_count: number;
  is_going: boolean;
  is_liked: boolean;
  comments: CommentJson[];
}

export class AppEvent {
  id: number;
  name: string;
  beginTime: number;
  endTime: number;
  description: string;
  creator: User;
  createTime: number;
  updateTime: number;
  channel: Channel;
  goings: User[];
  likes: User[];
  images: string[];
  location: string;
  goingsCount: number;
  likesCount: number;
  isGoing: boolean;
  isLiked: boolean;
  comments: Comment[];

  constructor(data: AppEventJson) {
    this.id = data.id;
    this.name = data.name;
    this.beginTime = data.begin_time;
    this.endTime = data.end_time;
    this.description = data.description;
    this.creator = User.fromData(data.creator);
    this.createTime = data.create_time;
    this.updateTime = data.update_time;
    this.channel = Channel.fromData(data.channel);
    this.goings = User.listFromData(data.goings);
    this.likes = User.listFromData(data.likes);
    this.images = data.images;
    this.location = data.location;
    this.goingsCount = data.goings_count;
    this.likesCount = data.likes_count;
    this.isGoing = data.is_going;
    this.isLiked = data.is_liked;
    this.comments = Comment.listFromData(data.comments);
  }

  toJson(): AppEventJson {
    return {
      id: this.id,
      name: this.name,
      begin_time: this.beginTime,
      end_time: this.endTime,
      description: this.description,
      creator: this.creator.toJson(),
      create_time: this.createTime,
      update_time: this.updateTime,
      channel: this.channel.toJson(),
      goings: User.toJsonList(this.goings),
      likes: User.toJsonList(this.likes),
      images: this.images,
      location: this.location,
      goings_count: this.goingsCount,
      likes_count: this.likesCount,
      is_going: this.isGoing,
      is_liked: this.isLiked,
      comments: Comment.toJsonList(this.comments),
    };
  }

  static fromData(data: AppEventJson): AppEvent {
    return new AppEvent(data);
  }

  static listFromData(dataList: AppEventJson[]): AppEvent[] {
    return dataList.map(data => new AppEvent(data));
  }

  static toJsonList(eventList: AppEvent[]): AppEventJson[] {
    return eventList.map(event => event.toJson());
  }
}
