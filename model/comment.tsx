import {User, UserJson} from './user';

export interface CommentJson {
  id: number;
  create_time: number;
  comment: string;
  author: UserJson;
}

export class Comment {
  id: number;
  createTime: number;
  comment: string;
  author: User;

  constructor(data: CommentJson) {
    this.id = data.id;
    this.createTime = data.create_time;
    this.comment = data.comment;
    this.author = User.fromData(data.author);
  }

  toJson(): CommentJson {
    return {
      id: this.id,
      create_time: this.createTime,
      comment: this.comment,
      author: this.author,
    };
  }

  static fromData(data: CommentJson): Comment {
    return new Comment(data);
  }

  static listFromData(dataList: CommentJson[]): Comment[] {
    return dataList.map(data => Comment.fromData(data));
  }

  static toJsonList(commentsList: Comment[]): CommentJson[] {
    return commentsList.map(comment => comment.toJson());
  }
}
