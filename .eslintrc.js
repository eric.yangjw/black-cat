module.exports = {
  root: true,
  extends: [
    'standard-with-typescript', // Installed in step 2
    'eslint-config-prettier',
  ],
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint', // Installed in step 2
    'react', // Installed in step 1
    'react-native', // Installed in step 1
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    project: './tsconfig.json', // Required for Standard plugin
  },
  env: {
    'react-native/react-native': true,
  },
  rules: {
    'prettier/prettier': 'off', // Turn off normal prettier
    // Any rules you want to add/turn off come here...
    // '@typescript-eslint/no-var-requires': 0,
    'default-param-last': 'off',
    '@typescript-eslint/default-param-last': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/restrict-template-expressions': 'off',
    'strict-boolean-expressions': [0, 'allowAny'],
    '@typescript-eslint/strict-boolean-expressions': 'off',
    '@typescript-eslint/naming-convention': 'off',
    '@typescript-eslint/prefer-optional-chain': 'off',
  },
  overrides: [
    {
      files: ['*.tsx'],
      rules: {
        '@typescript-eslint/no-var-requires': 'off',
      },
    },
  ],
};
