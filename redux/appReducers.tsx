import {combineReducers} from 'redux';
import logInReducer from '../screen/login_screen/redux/authReducer';
import searchReducer from '../screen/drawer_screen/redux/searchReducer';
import profileReducer from '../screen/profile_screen/redux/profileReducer';
import feedReducer from '../screen/feed_screen/redux/feedReducer';
import eventReducer from '../screen/event_details_screen/redux/eventReducer';

const appReducers = combineReducers({
  auth: logInReducer,
  feed: feedReducer,
  profile: profileReducer,
  search: searchReducer,
  event: eventReducer,
});

export default appReducers;
