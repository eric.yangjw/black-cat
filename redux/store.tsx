import {createStore, applyMiddleware} from 'redux';
// import { configureStore } from '@reduxjs/toolkit'

import thunk from 'redux-thunk';
import appReducers from './appReducers';
import {composeWithDevTools} from 'redux-devtools-extension';

const composeEnhancers = composeWithDevTools({});

const store = createStore(
  appReducers,
  composeEnhancers(applyMiddleware(thunk)),
);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
