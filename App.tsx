import React from 'react';

import {Provider} from 'react-redux';
import store from './redux/store';
import {RootSiblingParent} from 'react-native-root-siblings';

import {
  useFonts,
  SourceSansPro_400Regular,
  SourceSansPro_600SemiBold,
  SourceSansPro_700Bold,
} from '@expo-google-fonts/source-sans-pro';
import AppLoading from 'expo-app-loading';
import AppNavigator from './navigation/AppNavigator';

const App = () => {
  const [fontsLoaded] = useFonts({
    SourceSansPro_400Regular,
    SourceSansPro_600SemiBold,
    SourceSansPro_700Bold,
  });

  if (!fontsLoaded) return <AppLoading />;

  return (
    <RootSiblingParent>
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    </RootSiblingParent>
  );
};

export default App;
