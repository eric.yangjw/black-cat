import React, {FC} from 'react';
import {View} from 'react-native';
import LogoCat from '../assets/svg/logo-cat.svg';
import LogoPassword from '../assets/svg/password.svg';
import LogoSearch from '../assets/svg/search.svg';
import LogoUser from '../assets/svg/user.svg';
import LogoTime from '../assets/svg/time.svg';

import LogoCheckOutline from '../assets/svg/check-outline.svg';
import LogoCheck from '../assets/svg/check.svg';
import LogoLikeOutline from '../assets/svg/like-outline.svg';
import LogoLike from '../assets/svg/like.svg';

import LogoDetails from '../assets/svg/info.svg';
import LogoDetailsOutline from '../assets/svg/info-outline.svg';
import LogoParticipants from '../assets/svg/people.svg';
import LogoParticipantsOutline from '../assets/svg/people-outline.svg';
import LogoComments from '../assets/svg/comment.svg';
import LogoCommentsOutline from '../assets/svg/comment-outline.svg';

import LogoFromDate from '../assets/svg/date-from.svg';
import LogoToDate from '../assets/svg/date-to.svg';
import LogoReply from '../assets/svg/reply.svg';
import LogoSingleComment from '../assets/svg/comment-single.svg';

import LogoCross from '../assets/svg/cross.svg';
import LogoSend from '../assets/svg/send.svg';

import LogoNoActivity from '../assets/svg/no-activity.svg';
import LogoEmail from '../assets/svg/email.svg';

import LogoPast from '../assets/svg/past.svg';
import LogoPastOutline from '../assets/svg//past-outline.svg';

import LogoHome from '../assets/svg/home.svg';

interface LogoProps {
  color?: string;
  width?: number;
  height?: number;
}
const BlackCatLogo: FC<LogoProps> = ({
  color = '#D5EF7F',
  width = 32,
  height = 32,
}: LogoProps) => (
  <View>
    <LogoCat width={width} height={width} fill={color} />
  </View>
);

const UserLogo: FC<LogoProps> = ({
  color = '#BABABA',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingHorizontal: 5}}>
    <LogoUser width={width} height={width} fill={color} />
  </View>
);

const PasswordLogo: FC<LogoProps> = ({
  color = '#BABABA',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingHorizontal: 5}}>
    <LogoPassword width={width} height={width} fill={color} />
  </View>
);

const SearchLogo: FC<LogoProps> = ({
  color = '#453257',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingHorizontal: 5}}>
    <LogoSearch width={width} height={width} fill={color} />
  </View>
);

const TimeLogo: FC<LogoProps> = ({
  color = '#8560A9',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoTime width={width} height={width} fill={color} />
  </View>
);

const CheckOutlineLogo: FC<LogoProps> = ({
  color = '#AC8EC9',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoCheckOutline width={width} height={width} fill={color} />
  </View>
);

const CheckLogo: FC<LogoProps> = ({
  color = '#AECB4F',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoCheck width={width} height={width} fill={color} />
  </View>
);

const LikeOutlineLogo: FC<LogoProps> = ({
  color = '#AC8EC9',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoLikeOutline width={width} height={width} fill={color} />
  </View>
);

const LikeLogo: FC<LogoProps> = ({
  color = '#FF5C5C',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoLike width={width} height={width} fill={color} />
  </View>
);

const DetailsOutlineLogo: FC<LogoProps> = ({
  color = '#BABABA',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoDetailsOutline width={width} height={width} fill={color} />
  </View>
);

const DetailsLogo: FC<LogoProps> = ({
  color = '#AECB4F',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoDetails width={width} height={width} fill={color} />
  </View>
);

const ParticipantsOutlineLogo: FC<LogoProps> = ({
  color = '#BABABA',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoParticipantsOutline width={width} height={width} fill={color} />
  </View>
);

const ParticipantsLogo: FC<LogoProps> = ({
  color = '#AECB4F',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoParticipants width={width} height={width} fill={color} />
  </View>
);

const CommentsOutlineLogo: FC<LogoProps> = ({
  color = '#BABABA',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoCommentsOutline width={width} height={width} fill={color} />
  </View>
);

const CommentsLogo: FC<LogoProps> = ({
  color = '#AECB4F',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoComments width={width} height={width} fill={color} />
  </View>
);

const FromDateLogo: FC<LogoProps> = ({
  color = '#AECB4F',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoFromDate width={width} height={width} fill={color} />
  </View>
);

const ToDateLogo: FC<LogoProps> = ({
  color = '#AECB4F',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoToDate width={width} height={width} fill={color} />
  </View>
);

const ReplyLogo: FC<LogoProps> = ({
  color = '#D5EF7F',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoReply width={width} height={width} fill={color} />
  </View>
);

const SingleCommentLogo: FC<LogoProps> = ({
  color = '#49355C',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoSingleComment width={width} height={width} fill={color} />
  </View>
);

const CrossLogo: FC<LogoProps> = ({
  color = '#49355C',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoCross width={width} height={width} fill={color} />
  </View>
);

const SendLogo: FC<LogoProps> = ({
  color = '#49355C',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoSend width={width} height={width} fill={color} />
  </View>
);

const NoActivityLogo: FC<LogoProps> = ({
  color = '#D3C1E5',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoNoActivity width={width} height={width} fill={color} />
  </View>
);

const EmailLogo: FC<LogoProps> = ({
  color = '#8560A9',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoEmail width={width} height={width} fill={color} />
  </View>
);

const PastOutlineLogo: FC<LogoProps> = ({
  color = '#BABABA',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoPastOutline width={width} height={width} fill={color} />
  </View>
);

const PastLogo: FC<LogoProps> = ({
  color = '#AECB4F',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoPast width={width} height={width} fill={color} />
  </View>
);

const HomeLogo: FC<LogoProps> = ({
  color = '#453257',
  width = 14,
  height = 14,
}: LogoProps) => (
  <View style={{paddingRight: 5}}>
    <LogoHome width={width} height={width} fill={color} />
  </View>
);

export {
  BlackCatLogo,
  UserLogo,
  PasswordLogo,
  SearchLogo,
  TimeLogo,
  CheckLogo,
  CheckOutlineLogo,
  LikeLogo,
  LikeOutlineLogo,
  DetailsLogo,
  DetailsOutlineLogo,
  ParticipantsLogo,
  ParticipantsOutlineLogo,
  CommentsLogo,
  CommentsOutlineLogo,
  FromDateLogo,
  ToDateLogo,
  ReplyLogo,
  SingleCommentLogo,
  CrossLogo,
  SendLogo,
  NoActivityLogo,
  EmailLogo,
  PastLogo,
  PastOutlineLogo,
  HomeLogo,
};
