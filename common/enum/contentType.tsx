export enum ProfileContentType {
  Liked = 'liked',
  Going = 'going',
  Past = 'past',
}

export enum EventContentType {
  Details = 'Details',
  Participants = 'Participants',
  Comments = 'Comments',
}
