export enum DateType {
  ANYTIME = 'any time',
  TODAY = 'today',
  TOMORROW = 'tomorrow',
  THIS_WEEK = 'this week',
  THIS_MONTH = 'this month',
  LATER = 'later',
  NONE = '',
}
