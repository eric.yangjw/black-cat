# Backend API Documentation

## Starting node server

Start the node server first.

    cd backend
    npm run startDev

## End Points

#### Initialise Backend Data

A database is not used. Therefore, calling this endpoint sets up the necessary sample data for the project.

    GET http://localhost:3000/

#### Log In

Authenicate user with the given username and password.

    POST http://localhost:3000/login
    {
        username: String
        password: String
    }

| Status Code | Description                                                                                                           |
| ----------- | --------------------------------------------------------------------------------------------------------------------- |
| 200         | Username and password given is correct. ` { userId: number, userAvatarId: number, username: string}` will be returned |
| 401         | Either the username or password is invalid. Error message will be returned.                                           |

#### Fetch User Data

Once the user is authenticated, the data related to the user is fetched.

    POST http://localhost:3000/user
    {
        userId: number
    }

| Status Code | Description                                                   |
| ----------- | ------------------------------------------------------------- |
| 200         | User ID found in the database and the user found is returned. |
| 404         | User ID not found. No data is returned.                       |

#### Toggle Like Status of an Activity

Toggle the user's like status for the activity. If the user likes the activity, his avatarId will be added to the `likeList` and his `hasLiked` status will be set to `true`. Else, the user's avatartId will be removed from the `likeList` and his `hasLiked` status will be set to `false`.

    POST http://localhost:3000/user
    {
        userId: number,
        activityId: string,
        hasLiked: boolean
    }

| Status Code | Description                                                                           |
| ----------- | ------------------------------------------------------------------------------------- |
| 200         | User's like status and like list is updated.                                          |
| 404         | Either user or activity is not found. Error message is returned in the response body. |

#### Toggle Going Status of an Activity

Toggle the user's going status for the activity. If the user is going to the activity, his avatarId will be added to the `goingList` and his `isGoing` status will be set to `true`. Else, the user's avatartId will be removed from the `goingList` and his `isGoing` status will be set to `false`.

    POST http://localhost:3000/user
    {
        userId: number,
        activityId: string,
        isGoing: boolean
    }

| Status Code | Description                                                                           |
| ----------- | ------------------------------------------------------------------------------------- |
| 200         | User's going status and going list is updated.                                        |
| 404         | Either user or activity is not found. Error message is returned in the response body. |

#### Add comment to an Activity

Add user's comment to an activity

    POST http://localhost:3000/add_comment
    {
        userId: number,
        activityId: string,
        isGoing: {
            commenterAvatarId: number,
            commenterName: string,
            commentedTime: string,
            comment: string
        }
    }

| Status Code | Description                                                                           |
| ----------- | ------------------------------------------------------------------------------------- |
| 200         | User's comment added successfully                                                     |
| 404         | Either user or activity is not found. Error message is returned in the response body. |

# Frontend React Native Documentation

## Starting expo

Ensure that you are in the root directory.

    npm run ios

## Logging in

Use the following credentials to log into the application.
Username: Shopee
Password: password

If the application prompt that it is an invalid password, reload the expo application by pressing "r" to initialise the dataset.

## Navigating around the application

Upon log in, the Feed Page is loaded. It shows the list of events posted by other members. User can browser the list of events.
User can click on the activity/event card and it will navigate into the specific activity/event where he can see the title, descriptions, event photos, event date and location, people who has participated in, people who liked, and comments.
User can click on the avatar icon on the top right corner to log out of the application.
Changes made should be reflected when the user log in again.

[x] Filter by date ranges and channels
[x] Infinite-scroll behavior of the event list

## Git Version Control

Can go to `https://gitlab.com/eric.yangjw/black-cat` to clone the project and make PR to the project.
[x] Gitlab pipeline is not set up

## Redux

File structure of redux state management.

```
BlackCat
└───redux
│   └───actions
|       |   authActions.tsx
|       |   feedActions.tsx
|       |   index.tsx
│   └───reducers
│       |   authReducer.tsx
|       |   feedReducer.tsx
|       |   index.tsx
│   store.tsx
...
```
